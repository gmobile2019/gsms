<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/jquery.jqplot.min.css"/>
<script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery.jqplot.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jqplot.pieRenderer.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jqplot.barRenderer.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jqplot.categoryAxisRenderer.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jqplot.pointLabels.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jqplot.canvasAxisLabelRenderer.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jqplot.canvasAxisTickRenderer.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jqplot.canvasTextRenderer.js"></script>
<script type="text/javascript">
    
    $(document).ready(function(){
        var arr=[];
        var org=<?php echo $this->session->userdata('org'); ?>;
        $.ajax({
            type:'POST',
            url:'<?php echo site_url('Admin/getPieData'); ?>',
            data:{org :org},
            success:function(data){
                    var dt=data.split(",");
                    
                    for(var i=0;i<dt.length;i++){
                        
                        var dt2=dt[i].split("::");
                        var arrTemp=[dt2[0],parseFloat(dt2[1])];
                        
                        arr.push(arrTemp);
                        
                    }
                    
                    var plot8 = $.jqplot('chart',[arr], {
                                        title:'Sent Messages',
                                        grid: {
                                            drawBorder: false, 
                                            drawGridlines: false,
                                            background: '#ffffff',
                                            shadow:false
                                        },
                                        axesDefaults: {

                                        },
                                        seriesDefaults:{
                                            renderer:$.jqplot.PieRenderer,
                                            rendererOptions: {
                                                showDataLabels: true
                                            }
                                        },
                                        legend: {
                                            show: true,
                                            rendererOptions: {
                                                numberRows: 1
                                            },
                                            location: 's'
                                        }
                                    });
             }

            });
                    
        //pending files
        $.ajax({
            type:'POST',
            url:'<?php echo site_url('Admin/getPendingFileData'); ?>',
            data:{org :org},
            success:function(data){
                    $('div#files').empty();
                    $('div#files').html(data);
             }

            });
    });
    
</script>
<div class="row">
    <div class="col-4" id="chart" style="height:400px;width:400px; "></div>
    <div class="col-8" id="files"></div>
    <div class="col-3"></div>
</div>