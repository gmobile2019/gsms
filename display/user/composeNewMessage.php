<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div id="divform">
    <?php 
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open_multipart('User/composeNewMessage/'); 
                ?>
                
                <div class="form-group row">
                    <label for="message" class="col-4 control-label">Message&nbsp;&nbsp;<span id="smsCount" style="font-weight: bold;font-style: italic">(0)</span></label>
                    <div class="col-3">
                        <textarea class="form-control" name="message" id="message" placeholder="Message Content" required><?php echo set_value('message'); ?></textarea>
                        <?php echo form_error('message'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="msgcategory" class="col-4 control-label">Message Category</label>
                    <div class="col-3">
                        <select name="msgcategory" id="msgcategory" class="form-control" required>
                            <option value="General" <?php echo set_select('msgcategory', 'General')?>>General</option>
                            <option value="Personalized" <?php echo set_select('msgcategory', 'Personalized')?>>Personalized</option>
                            
                        </select>
                        <?php echo form_error('msgcategory'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="sender" class="col-4 control-label">Message Sender</label>
                    <div class="col-3">
                        <select name="sender" id="sender" class="form-control" required>
                            <?php foreach($senders as $key=>$value){ ?>

                            <option value="<?php echo $value->SENDER; ?>" <?php echo set_select('sender',$value->SENDER); ?>><?php echo $value->SENDER; ?></option>

                                <?php } ?>
                        </select>
                        <?php echo form_error('sender'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="bulk" class="col-4 control-label">Are you doing a bulk upload?</label>
                    <div class="col-3">
                        <select name="bulk" id="bulk" class="form-control">
                            <option value="Yes" <?php echo set_select('bulk', 'Yes')?>>Yes</option>
                            <option value="No" <?php echo set_select('bulk', 'No')?>>No</option>
                        </select>
                        <?php echo form_error('bulk'); ?>
                    </div>
                </div>
                <div class="form-group row bulkUpload">
                    <label for="bulk_recipients" class="col-4 control-label">Upload File</label>
                    <div class="col-3">
                        <input type="file" class="form-control" name="bulk_recipients" id="bulk_recipients"/>
                    </div>
                    <div class="col-3">
                        <?php echo anchor('User/composeNewMessage/general','<span id="general" style="color:black" class="fa fa-download fa-1x" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="General Upload Template">Download General Upload Template</span>') ?>
                        &nbsp;&nbsp;
                        <?php echo anchor('User/composeNewMessage/personalized','<span id="personalized" style="color:black" class="fa fa-download fa-1x" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Personalized Upload Template">Download Personalized Upload Template</span>') ?>
                    </div>
                </div>
                <div class="form-group row fewUpload">
                    <label for="recipientsNo" class="col-4 control-label">How many recipients?</label>
                    <div class="col-3">
                        <select name="recipientsNo" id="recipientsNo" class="form-control">
                            <option value="1" <?php echo set_select('recipientsNo', '1')?>>1</option>
                            <option value="2" <?php echo set_select('recipientsNo', '2')?>>2</option>
                            <option value="3" <?php echo set_select('recipientsNo', '3')?>>3</option>
                            <option value="4" <?php echo set_select('recipientsNo', '4')?>>4</option>
                        </select>
                        <?php echo form_error('recipientsNo'); ?>
                    </div>
                </div>
                <div class="form-group row fewUpload" id="fewPersonalized">
                    <label for="variableNo" class="col-4 control-label">How many variables are present in the message?</label>
                    <div class="col-3">
                        <select name="variableNo" id="variableNo" class="form-control">
                            <option value="1" <?php echo set_select('variableNo', '1')?>>1</option>
                            <option value="2" <?php echo set_select('variableNo', '2')?>>2</option>
                            <option value="3" <?php echo set_select('variableNo', '3')?>>3</option>
                            <option value="4" <?php echo set_select('variableNo', '4')?>>4</option>
                        </select>
                        <?php echo form_error('variableNo'); ?>
                    </div>
                </div>
                <div class="form-group row fewUpload" id="recipients"></div>
                <div class="form-group row">
                    <label for="now" class="col-4 control-label">Do you want to send message Instantly?</label>
                    <div class="col-3">
                        <select name="now" id="now" class="form-control" required>
                            <option value="Yes" <?php echo set_select('now', 'Yes')?>>Yes</option>
                            <option value="No" <?php echo set_select('now', 'No')?>>No</option>
                        </select>
                        <?php echo form_error('now'); ?>
                    </div>
                </div>
                <div class="form-group row scheduleSending">
                    <label for="scheduleNo" class="col-4 control-label">How many schedules?</label>
                    <div class="col-3">
                        <select name="scheduleNo" id="scheduleNo" class="form-control">
                            <option value="1" <?php echo set_select('scheduleNo', '1')?>>1</option>
                            <option value="2" <?php echo set_select('scheduleNo', '2')?>>2</option>
                            <option value="3" <?php echo set_select('scheduleNo', '3')?>>3</option>
                            <option value="4" <?php echo set_select('scheduleNo', '4')?>>4</option>
                        </select>
                        <?php echo form_error('scheduleNo'); ?>
                    </div>
                </div>
                <div class="form-group row scheduleSending" id="schedules"></div>
                <div class="form-group">
                    <div class="offset-3 col-xs-12 col-4 btn btn-link">
                        <button type="submit" class="btn btn-success">Save Information</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>
