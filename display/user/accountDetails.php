<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
    
    $(document).ready(function(){
        
    });
</script>
<table class="table table-bordered table-hover table-condensed table-dark">
    <tbody>
    <tr>
        <th colspan="2" style="text-align: center">Basic Details</th>
    </tr>
    <tr>
        <th>&nbsp;&nbsp;Organization Name</th>
        <td>&nbsp;&nbsp;<?php echo $data['orgName'];?></td>
    </tr>
    <tr>
        <th>&nbsp;&nbsp;Organization Address</th>
        <td>&nbsp;&nbsp;<?php echo $data['orgAddress'];?></td>
    </tr>
    <tr>
        <th>&nbsp;&nbsp;Phone Contact</th>
        <td>&nbsp;&nbsp;<?php echo $data['orgPhone'];?></td>
    </tr>
    <tr>
        <th>&nbsp;&nbsp;Email Contact</th>
        <td>&nbsp;&nbsp;<?php echo $data['orgEmail'];?></td>
    </tr>
    <tr>
        <th colspan="2" style="text-align: center">Operational Details</th>
    </tr>
    <tr>
        <th>&nbsp;&nbsp;Message Balance</th>
        <td>&nbsp;&nbsp;<?php echo $data['messageBalance'];?></td>
    </tr>
    <tr>
        <th>&nbsp;&nbsp;Portal Credentials</th>
        <td>
            <ul style="list-style: none">
                <li>Username : <?php echo $data['portalUsername']; ?></li>
                <li>Password : <?php echo $data['portalPss']; ?></li>
            </ul>
        </td>
    </tr>
    <tr>
        <th>&nbsp;&nbsp;Web Service Credentials</th>
        <td>
            <ul style="list-style: none">
                <li>Username : <?php echo $data['apiUsername']; ?></li>
                <li>Password : <?php echo $data['apiPassword']; ?></li>
                <li>Token : <?php echo $data['apiToken']; ?></li>
            </ul>
        </td>
    </tr>
    <tr>
        <th>&nbsp;&nbsp;Active Sender IDs</th>
        <td>
            <ul style="list-style-type:disc">
                <?php foreach($data['senders'] as $value){ ?>
                
                <li><?php echo $value; ?></li>
                
                <?php }?>
            </ul>
        </td>
    </tr>
    
    </tbody>
</table>