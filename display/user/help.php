<div class="row" style="font-family: monospace">
    
    <div class="col-12"><h3 style="text-align: center;">User Manual</h3></div>
    <div class="col-12">
        <ul style="list-style: disc;text-decoration: underline;">
            <li><a href="#home">Home</a></li>
            <li><a href="#message">Messages</a></li>
            <li><a href="#subscription">Subscriptions</a></li>
            <li><a href="#report">Report</a></li>
            <li><a href="#account">Account</a></li>
        </ul>
        
    </div>
    <div class="col-12" id="home">
        <h5 style="font-style: italic;font-weight: bold">Home</h5>
        <p>
            A quick statistical view on the uploaded message status.The statistics is represented by a PIE chart showing distribution of messages percentage wise in different status.
        </p>
    </div>
    <div class="col-12" id="message">
        <h5 style="font-style: italic;font-weight: bold;text-decoration: underline">Messages</h5>
        <p>
            A paginated page with uploaded messages with <span style="font-style: oblique;font-weight: bold">organization name, message ID, number of messages, recipient, sender, schedule time and status </span> can be viewed once the <span style="font-style: oblique;font-weight: bold">Messages link</span> has been clicked.
        </p>
        <p>
            In order to refine the messages view a search filter exists at the top of the table with <span style="font-style: oblique;font-weight: bold">recipient, starting date of which messages were uploaded, ending date of which messages were uploaded and status </span> respectively. 
            <img style="border-radius: 5px" alt="searchFilterLink" src="<?php echo base_url()?>img/help/searchFilter.jpg"/>
        </p>
        <p>
            A <span style="font-style: oblique;font-weight: bold">download link</span> exists just below the search filter in-cases where a document is needed for various uses. The link is as seen below:-
            <img style="border-radius: 5px" alt="downloadLink" src="<?php echo base_url()?>img/help/exportLink.jpg"/>
        </p>
        <p>
            When needed to send messages to recipients click on the Compose New Message link just beneath the export link.
            <img style="border-radius: 5px" alt="composeMessageLink" src="<?php echo base_url()?>img/help/composeMessageLink.jpg"/>
        </p>
        <h6 style="font-style: italic;text-decoration: underline;font-weight: bold">Composing New Message</h6>
        <p>
            <div>
                <span style="font-style: oblique;text-decoration: underline;">General Message Category</span>
                This message intends to deliver the same information to all the recipients<br/>
                <span style="font-style: oblique;font-weight: bold"> Example: We are happy to inform you that we are having discounts for all our products</span>
            </div>
            <div>
                <span style="font-style: oblique;text-decoration: underline;">Personalized Message Category</span>
                This is a message that intends to deliver information to a singled out person. This message has different variables that are to be replaced with a specific wording/information to address one particular person.<br/>
                <span style="font-style: oblique;font-weight: bold">
                Example : 
                A clearing and forwarding agency intends to notify it clients that there goods have arrived hence they need to go to the offices for payment and pickup on a particular date and time. Client A goes by the name Hans, he needs to go to the office with $2000 on 22nd September at 09am to pick up Television set. Client B goes by the name Lisa, he needs to go to the office with $200 on 23rd September at 09am to pick up a pair of shoes. Client C goes by the name Eugene, he needs to go to the office with $60 on 24th September at 09am to pick up headphone set.
                <br/>On the above case we have 3 clients each having 4 variables (name, amount of money, date and package)<br/>
                Message to each client<br/>
                Client A :<br/> 
                Hello Hans, your Television set has safely arrived. Please come to our offices on 22nd of September at 09am with $2000 for pickup.<br/>
                Client B:<br/> 
                Hello Lisa, your pair of shoes has safely arrived. Please come to our offices on 23nd of September at 09am with $200 for pickup.<br/>
                Client C:<br/> 
                Hello Eugene, your headphone set has safely arrived. Please come to our offices on 24nd of September at 09am with $60 for pickup.<br/>
                Template message:<br/>
                Hello %s, your %s has safely arrived. Please come to our offices on %s at 09am with %s for pickup.</span>
            </div>
        </p>
        <p>
            This page consists of different form fields essential to compose and send a text message to recipients.<span style="font-style: oblique;font-weight: bold"> Message field, message category field, message sender field, bulk upload option field, file upload field, number of recipients field, number of message variables field {in cases of personalized message category}, recipient field(s), 
        variable(s) fields {in cases of personalized message category}, option for a scheduled or instant sending field, 
        date field and time field {24 hours format}</span> are all available to be used in-case of needing to compose and send message.
        </p>
        <p>
            Depending on the <span style="font-style: oblique;font-weight: bold">category of the message, upload option i.e Personalized or General and the timing of sending  the message i.e instantly or scheduled </span> not all fields are mandatory hence depending on the aformentioned only the necessary fields will appear.
        <div class="row">
            <div class="col-6">
                <img style="border-radius: 5px" alt="bulkSmsInstant" src="<?php echo base_url()?>img/help/bulkSmsInstant.jpg"/>
                <div style="font-style: oblique;">Bulk SMS Composition view with Instant sending </div>
            </div>
            <div class="col-6">
                <img style="border-radius: 5px" alt="bulkSmsSchedule" src="<?php echo base_url()?>img/help/bulkSmsSchedule.jpg"/>
                <div style="font-style: oblique;">Bulk SMS Composition view with Scheduled sending </div>
            </div>
            <div class="col-6">
                <img style="border-radius: 5px" alt="multipleGenInstant" src="<?php echo base_url()?>img/help/multipleGenInstant.jpg"/>
                <div style="font-style: oblique;">Multiple General SMS Composition view with Instant sending</div>
            </div>
            <div class="col-6">
                <img style="border-radius: 5px" alt="multipleGenSchedule" src="<?php echo base_url()?>img/help/multipleGenSchedule.jpg"/>
                <div style="font-style: oblique;">Multiple General SMS Composition view with scheduled sending</div>
            </div>
            <div class="col-6">
                <img style="border-radius: 5px" alt="multiplePersoInstant" src="<?php echo base_url()?>img/help/multiplePersoInstant.jpg"/>
                <div style="font-style: oblique;">Multiple Personalized SMS Composition view with Instant sending</div>
            </div>
            <div class="col-6">
                <img style="border-radius: 5px" alt="multiplePersoSchedule" src="<?php echo base_url()?>img/help/multiplePersoSchedule.jpg"/>
                <div style="font-style: oblique;">Multiple Personalized SMS Composition view with scheduled sending</div>
            </div>
        </div>
        </p>
    </div>
    <div class="col-12" id="subscription">
        <h5 style="font-style: italic;font-weight: bold">Subscriptions</h5>
        Message subscription/top up history can be seen on this page.
    </div>
    <div class="col-12" id="report">
        <h5 style="font-style: italic;font-weight: bold">Report</h5>
        Aggregated report of the messages grouped in-terms of <span style="font-style: oblique;font-weight: bold">Organization Name, Status and Number of messages.</span>
        <img style="border-radius: 5px" alt="report" src="<?php echo base_url()?>img/help/report.jpg"/>
    </div>
    <div class="col-12" id="account">
        <h5 style="font-style: italic;font-weight: bold">Account</h5>
        All the basic account details can be found here.<br/>Information such as <span style="font-style: oblique;font-weight: bold">Organization name,Organization address, Organization phone contact, Organization email contact, message balance, portal credentials, web interface credentials and active sender Ids.</span>
    </div>
    
</div>

