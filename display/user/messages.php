<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div  class="row">
    <div class="offset-2">
        <?php 
                    $attributes = array('class' => 'form-inline','role'=>'form');
                    echo form_open('User/messages',$attributes); 
                ?>
                    <div class="form-group">
                        <label class="sr-only" for="recipient"></label>
                        <input type="text" class="form-control" name="recipient" id="recipient" placeholder="Recipient" value="<?php echo $recipient; ?>" />
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="start"></label>
                        <input type="date" class="form-control" name="start" id="start" placeholder="Start Date" value="<?php echo $start; ?>" />
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="end"></label>
                        <input type="date" class="form-control" name="end" id="end" placeholder="End Date" value="<?php echo $end; ?>" />
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="status"></label>
                        <select name="status" id="status" class="form-control">
                            <option value="">All Status</option>
                            <?php foreach($msg_status as $key=>$value){ ?>

                            <option value="<?php echo $value->STATUS; ?>" <?php echo $status == $value->STATUS?"selected='selected'":""; ?>><?php echo $value->STATUS; ?></option>

                                <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                       <input type="submit" class="btn btn-success" name="search"value="Search" /> 
                    </div>
        
            <?php echo form_close(); ?>
    </div>
</div>
<div class="row" style="padding: 10px">
    <div class="offset-5">
        <?php echo anchor("User/messages/org_".$org."_start_".$start."_end_".$end."_status_".$status."_recipient_".$recipient."_doc_2", '<span id="excel" class="fa fa-download fa-1x" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Excel">export</span>')?>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <table class="table table-condensed table-hover table-bordered">

        <thead>
            <tr>
                <th colspan="8" style="text-align:center;"><?php echo anchor('User/composeNewMessage','Compose New Message'); ?></th>
             </tr>
            <tr>
                <th style="text-align:center;width:50px">S/NO</th>
                <th style="text-align:center;width">Organization</th>
                <th style="text-align:center;width">Message ID</th>
                <th style="text-align:center;width">No Messages</th>
                <th style="text-align:center;width">Recipient</th>
                <th style="text-align:center;width">Sender</th>
                <th style="text-align:center;width">Schedule Time</th>
                <th style="text-align:center;width">Status</th>
             </tr>
        </thead>
        <tbody>
            <?php if($data != null){
                
                if($per_page == null){
                        $i=1;
                    }else{
                        $i=$per_page+1;
                    }
                foreach($data as $key=>$value){ ?>
                    <tr aria-hidden="true" data-toggle="tooltip" data-placement="top" title="<?php echo $value->SMS; ?>">
                        <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->NAME; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->MESSAGEID; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->MESSAGECOUNT; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->MSISDN; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->SENDERID; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->SCHEDULETIME; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->STATUS; ?></td>
                    </tr>  
                <?php }
                }else{ ?>
            <tr>
                <td colspan="8" style="text-align:center"> NO DATA FOUND</td>
            </tr>  
                <?php } ?>
        </tbody>
    </table>
</div>
<div class="offset-4 col-8">
    <?php echo $links; ?>
</div>
<div class="col-12">
    <ul style="list-style: none;font-weight: bold;font-size: 85%">
        <?php foreach($msg_status as $key=>$value){ ?>

        <li>**<?php echo $value->STATUS; ?> >> <span style="color:green;font-style: oblique;"><?php echo $value->DESCRIPTION; ?></span> </li>

        <?php } ?>
    </ul>>
</div>
</div>
