<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/bootstrap.css">
        <link rel="stylesheet" href="<?php echo base_url();?>styles/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/custom.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/jquery-ui.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/jquery.animatedheadline.css"/>
        <link rel="shortcut icon" href="<?php echo base_url();?>img/title.ico" />
        <script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery.js" ></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>styles/js/bootstrap.js" ></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery-blink.js" language="javscript"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery-ui.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery-ui.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery.animatedheadline.js"></script>
        <title>SMS</title>
        <script type="text/javascript">
            
            $(document).ready(function(){
                $('#banner').animatedHeadline();
                $('#banner').animatedHeadline({
                    animationType: "loading-bar",
                    animationDelay: 2500,
                    barAnimationDelay: 3800,
                    barWaiting: 800,
                    lettersDelay: 50,
                    typeLettersDelay: 150,
                    selectionDuration: 500,
                    typeAnimationDelay: 1300,
                    revealDuration: 600,
                    revealAnimationDelay: 1500
                });
            });
            
        </script>
    </head>
    <body id="divform">
        <div class="container-fluid">
            <div class="row" style="padding-bottom: 30px">
                <div class="col-9" id="banner">
                    <h1 class="ah-headline">
                        <span></span>
                        <span class="ah-words-wrapper">
                          <b class="is-visible">G-MOBILE BULK SMS</b>
                          <b>G-MOBILE BULK SMS</b>
                          <b>G-MOBILE BULK SMS</b>
                          <b>G-MOBILE BULK SMS</b>
                          <b>G-MOBILE BULK SMS</b>
                        </span>
                    </h1>
                </div>
                
            </div>
            <div class="row">
                <div class="col-5">
                    <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner" role="listbox">
                            <div class="carousel-item active">
                              <img class="d-block img-fluid" id="imgslider" src="<?php echo base_url();?>img/sliders/image3.jpg" alt="First slide">
                            </div>
                            <div class="carousel-item">
                              <img class="d-block img-fluid" id="imgslider" src="<?php echo base_url();?>img/sliders/dse.png" alt="Second slide">
                            </div>
                            <div class="carousel-item">
                              <img class="d-block img-fluid" id="imgslider" src="<?php echo base_url();?>img/sliders/mcb.jpg" alt="Third slide">
                            </div>
                            <div class="carousel-item">
                              <img class="d-block img-fluid" id="imgslider" src="<?php echo base_url();?>img/sliders/image1.jpg" alt="Fourth slide"/>
                            </div>
                            <div class="carousel-item">
                              <img class="d-block img-fluid" id="imgslider" src="<?php echo base_url();?>img/sliders/image5.jpg" alt="Five slide">
                            </div>
                            
                            
                        </div>
                    </div>
                </div>
                <div class="col-7">
                    <input type="hidden" id="flag" value="<?php echo $flag?>"/>
                        <?php echo $message; ?>
                    <form id="myform1" method="POST" action="<?php echo base_url() ?>index.php/Auth/login">
                        <div class="row">
                            <h5 id="publicheader" class="offset-6 col-6">Users Login</h5>
                        </div>
                        <div class="form-group row">
                            <label for="usrnme" class="offset-4 col-2 col-form-label">Username</label>
                            <div class="col-4">
                                <input class="form-control" type="text" name="usrnme" id="usrnme" placeholder="Username" value="<?php echo set_value('usrnme'); ?>" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="pswd" class="offset-4 col-2 col-form-label">Password</label>
                            <div class="col-4">
                                <input class="form-control" type="password" name="pswd" id="pswd" placeholder="password" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="offset-6 col-4">
                                <input class="btn btn-outline-success btn-block" type="submit" value="Login"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col"></div>
                <div class="col" id="footer">G-Mobile Int&commat;<?php echo date('Y') ?>.All Rights Reserved</div>
            </div>
        </div>
    </body>
</html>