<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
 
   $(document).ready(function(){
       
   });
</script>
<div id="divform">
    <?php 
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open('Admin/report/'); 
                ?>
       
                <div class="form-group row">
                    <label for="startdate" class="col-4 control-label">Start Date</label>
                    <div class="col-3">
                        <input type="date" class="form-control" name="startdate" id="startdate" placeholder="Start Date" value="<?php echo set_value('startdate'); ?>" required/>
                        <?php echo form_error('startdate'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="enddate" class="col-4 control-label">End Date</label>
                    <div class="col-3">
                        <input type="date" class="form-control" name="enddate" id="enddate" placeholder="End Date" value="<?php echo set_value('enddate'); ?>" required/>
                        
                            <?php echo form_error('enddate'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="org" class="col-4 control-label">Organization</label>
                    <div class="col-3">
                        <select name="org" id="org" class="form-control">
                            <option value="">All Organizations</option>
                            <?php foreach($organizations as $key=>$value){ ?>

                            <option value="<?php echo $value->ID; ?>" <?php echo set_select('org',$value->ID); ?>><?php echo $value->NAME; ?></option>

                                <?php } ?>
                        </select>
                        <?php echo form_error('org'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="status" class="col-4 control-label">Status</label>
                    <div class="col-3">
                        <select name="status" id="status" class="form-control">
                            <option value="">All Status</option>
                            <?php foreach($msg_status as $key=>$value){ ?>

                            <option value="<?php echo $value->STATUS; ?>" <?php echo $status == $value->STATUS?"selected='selected'":""; ?>><?php echo $value->STATUS; ?></option>

                                <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="offset-3 col-xs-12 col-4 btn btn-link">
                        <button type="submit" class="btn btn-success">Generate Report</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>
