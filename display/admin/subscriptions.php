<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div  class="row">
    <div class="offset-4">
        <?php 
                    $attributes = array('class' => 'form-inline','role'=>'form');
                    echo form_open('Admin/subscriptions',$attributes); 
                ?>
                    <div class="form-group">
                        <label class="sr-only" for="start"></label>
                        <input type="date" class="form-control" name="start" id="start" placeholder="Start Date" value="<?php echo $start; ?>" />
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="end"></label>
                        <input type="date" class="form-control" name="end" id="end" placeholder="End Date" value="<?php echo $end; ?>" />
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="org"></label>
                        <select name="org" id="org" class="form-control">
                            <option value="">Organization</option>
                            <?php foreach($organizations as $key=>$value){ ?>

                            <option value="<?php echo $value->ID; ?>" <?php echo $org == $value->ID?"selected='selected'":""; ?>><?php echo $value->NAME; ?></option>

                                <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                       <input type="submit" class="btn btn-success" name="search"value="Search" /> 
                    </div>
        
            <?php echo form_close(); ?>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <table class="table table-condensed table-hover table-bordered">

        <thead>
            <tr>
                <th colspan="5" style="text-align:center;"><?php echo anchor('Admin/register_subscription','Top Up Messages'); ?></th>
             </tr>
            <tr>
                <th style="text-align:center;width:50px">S/NO</th>
                <th style="text-align:center;width:550px">Organization</th>
                <th style="text-align:center;width:200px">Message Count</th>
                <th style="text-align:center;width:250px">Date</th>
                <th style="text-align:center;width:250px">Subscription ID</th>
             </tr>
        </thead>
        <tbody>
            <?php if($data != null){
                
                if($per_page == null){
                        $i=1;
                    }else{
                        $i=$per_page+1;
                    }
                foreach($data as $key=>$value){ ?>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->NAME; ?></td>
                        <td>&nbsp;&nbsp;<?php echo number_format($value->MESSAGECOUNT); ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->LASTUPDATE; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->SUBSCRIPTIONID; ?></td>
                    </tr>  
                <?php }
                }else{ ?>
            <tr>
                <td colspan="5" style="text-align:center"> NO DATA FOUND</td>
            </tr>  
                <?php } ?>
        </tbody>
    </table>
</div>
<div class="offset-4 col-4">
    <?php echo $links; ?>
</div>
</div>
