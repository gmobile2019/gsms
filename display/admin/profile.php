<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
    
    $(document).ready(function(){
        var clickId;
        var id;
        var dataValue;
        var msg;
        
        $('span.fa-check-circle').hide();
        $('div.notshow').hide();
        
        $('span.fa-pencil-square-o').each(function(){
           
           $(this).click(function(){
               clickId=$(this).attr('id');
               id=clickId.split('_');
            
               $(this).fadeOut();
               $(this).parent().parent().parent().find('div#'+id[0]+'_dsply_'+id[3]).fadeOut();
               $(this).parent().parent().parent().find('div#'+id[0]+'_edit_'+id[3]).fadeIn();
               $('span#'+id[0]+'_save_'+id[3]).fadeIn();
               $('#'+id[0]+'_'+id[3]).focus();
           });
        });
        
        $('span.fa-check-circle').each(function(){
           
           $(this).click(function(){
               clickId=$(this).attr('id');
               id=clickId.split('_');
               dataValue=$('#'+id[0]+'_'+id[2]).val();
               
               $.ajax({
                type:'POST',
                url:'<?php echo site_url('Admin/save_profile'); ?>',
                data:{name:id[0],value:dataValue},
                success:function(data){
                      
                        if(data === "saved"){
                            $('div#'+id[0]+'_dsply_'+id[2]).empty();
                            $('div#'+id[0]+'_dsply_'+id[2]).text(dataValue);
                            msg='<span class="alert alert-success" role="alert" style="text-align:center;">data saved!</span>';;
                        }else{
                            msg=data;
                        }
                        
                        $('div#'+id[0]+'_edit_'+id[2]).fadeOut();
                        $('span#'+id[0]+'_edit_action_'+id[2]).fadeIn();
                        $('span#'+id[0]+'_save_'+id[2]).fadeOut();
                        $('div#'+id[0]+'_dsply_'+id[2]).fadeIn();
                        $('div#responseMessage').empty();
                        $('div#responseMessage').append(msg);
                 }

                });
               
           });
        });
        
        $(".dob").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd',
                yearRange: "-100:+0"
            });
    });
</script>
<table class="table table-bordered table-hover table-condensed table-dark">
    <tbody>
    <tr>
        <th>First Name</th>
        <td>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="fname_dsply_<?php echo $data->ID; ?>">
                    <?php echo $data->FIRST_NAME;?>
                </div>  
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 notshow" id="fname_edit_<?php echo $data->ID; ?>">
                    <input type="text" class="form-control" id="fname_<?php echo $data->ID; ?>" value="<?php echo $data->FIRST_NAME; ?>">
                </div>  
            </div>
        </td>
        <td>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <span class="fa fa-pencil-square-o fa-1x" style="cursor: pointer" id="fname_edit_action_<?php echo $data->ID; ?>"></span>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                <span class="fa fa-check-circle fa-1x" style="cursor: pointer" id="fname_save_<?php echo $data->ID; ?>"></span>
            </div>
        </td>
    </tr>
    <tr>
        <th>Middle Name</th>
        <td>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="mname_dsply_<?php echo $data->ID; ?>">
                    <?php echo $data->MIDDLE_NAME;?>
                </div>  
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 notshow" id="mname_edit_<?php echo $data->ID; ?>">
                    <input type="text" class="form-control" id="mname_<?php echo $data->ID; ?>" value="<?php echo $data->MIDDLE_NAME; ?>">
                </div>  
            </div>
        </td>
        <td>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <span class="fa fa-pencil-square-o fa-1x" style="cursor: pointer" id="mname_edit_action_<?php echo $data->ID; ?>"></span>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                <span class="fa fa-check-circle fa-1x" style="cursor: pointer" id="mname_save_<?php echo $data->ID; ?>"></span>
            </div>
        </td>
    </tr>
    <tr>
        <th>Surname</th>
        <td>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="surname_dsply_<?php echo $data->ID; ?>">
                    <?php echo $data->LAST_NAME;?>
                </div>  
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 notshow" id="surname_edit_<?php echo $data->ID; ?>">
                    <input type="text" class="form-control" id="surname_<?php echo $data->ID; ?>" value="<?php echo $data->LAST_NAME; ?>">
                </div>  
            </div>
        </td>
        <td>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <span class="fa fa-pencil-square-o fa-1x" style="cursor: pointer" id="surname_edit_action_<?php echo $data->ID; ?>"></span>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                <span class="fa fa-check-circle fa-1x " style="cursor: pointer" id="surname_save_<?php echo $data->ID; ?>"></span>
            </div>
        </td>
    </tr>
    <tr>
        <th>Email</th>
        <td>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="email_dsply_<?php echo $data->ID; ?>">
                    <?php echo $data->EMAIL;?>
                </div>  
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 notshow" id="email_edit_<?php echo $data->ID; ?>">
                    <input type="email" class="form-control" id="email_<?php echo $data->ID; ?>" value="<?php echo $data->EMAIL; ?>">
                </div>  
            </div>
        </td>
        <td>
           <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <span class="fa fa-pencil-square-o fa-1x" style="cursor: pointer" id="email_edit_action_<?php echo $data->ID; ?>"></span>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                <span class="fa fa-check-circle fa-1x" style="cursor: pointer" id="email_save_<?php echo $data->ID; ?>"></span>
            </div>
        </td>
    </tr>
    <tr>
        <th>Mobile No</th>
        <td>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="mobile_dsply_<?php echo $data->ID; ?>">
                    <?php echo $data->MSISDN;?>
                </div>  
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 notshow" id="mobile_edit_<?php echo $data->ID; ?>">
                    <input type="text" class="form-control" id="mobile_<?php echo $data->ID; ?>" value="<?php echo $data->MSISDN; ?>">
                </div>  
            </div>
        </td>
        <td>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <span class="fa fa-pencil-square-o fa-1x" style="cursor: pointer" id="mobile_edit_action_<?php echo $data->ID; ?>"></span>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                <span class="fa fa-check-circle fa-1x" style="cursor: pointer" id="mobile_save_<?php echo $data->ID; ?>"></span>
            </div>
        </td>
    </tr>
    <tr>
        <th>Username</th>
        <td>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="username_dsply_<?php echo $data->ID; ?>">
                    <?php echo $data->USERNAME;?>
                </div>  
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 notshow" id="username_edit_<?php echo $data->ID; ?>">
                    <input type="text" class="form-control" id="username_<?php echo $data->ID; ?>" value="<?php echo $data->USERNAME; ?>">
                </div>  
            </div>
        </td>
        <td></td>
    </tr>
    
    </tbody>
</table>

