<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/jquery.jqplot.min.css"/>
<script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery.jqplot.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jqplot.pieRenderer.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jqplot.barRenderer.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jqplot.categoryAxisRenderer.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jqplot.pointLabels.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jqplot.canvasAxisLabelRenderer.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jqplot.canvasAxisTickRenderer.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jqplot.canvasTextRenderer.js"></script>
<script type="text/javascript">
    
    $(document).ready(function(){
        var arr=[];
        var org=$('select[name=org]').val();
        $.ajax({
            type:'POST',
            url:'<?php echo site_url('Admin/getPieData'); ?>',
            data:{org :org},
            success:function(data){
                    var dt=data.split(",");
                    
                    for(var i=0;i<dt.length;i++){
                        
                        var dt2=dt[i].split("::");
                        var arrTemp=[dt2[0],parseFloat(dt2[1])];
                        
                        arr.push(arrTemp);
                        
                    }
                    
                    var plot8 = $.jqplot('chart',[arr], {
                                        title:'Sent Messages',
                                        grid: {
                                            drawBorder: false, 
                                            drawGridlines: false,
                                            background: '#ffffff',
                                            shadow:false
                                        },
                                        axesDefaults: {

                                        },
                                        seriesDefaults:{
                                            renderer:$.jqplot.PieRenderer,
                                            rendererOptions: {
                                                showDataLabels: true
                                            }
                                        },
                                        legend: {
                                            show: true,
                                            rendererOptions: {
                                                numberRows: 1
                                            },
                                            location: 's'
                                        }
                                    });
             }

            });
         
        $('select[name=org]').change(function(){
            var arr=[];
            var org=$(this).val();
            $.ajax({
            type:'POST',
            url:'<?php echo site_url('Admin/getPieData'); ?>',
            data:{org :org},
            success:function(data){
                    var dt=data.split(",");
                    
                    for(var i=0;i<dt.length;i++){
                        
                        var dt2=dt[i].split("::");
                        var arrTemp=[dt2[0],parseFloat(dt2[1])];
                        
                        arr.push(arrTemp);
                        
                    }
                    
                    var plot8 = $.jqplot('chart',[arr], {
                                        title:'Sent Messages',
                                        grid: {
                                            drawBorder: false, 
                                            drawGridlines: false,
                                            background: '#ffffff',
                                            shadow:false
                                        },
                                        axesDefaults: {

                                        },
                                        seriesDefaults:{
                                            renderer:$.jqplot.PieRenderer,
                                            rendererOptions: {
                                                showDataLabels: true
                                            }
                                        },
                                        legend: {
                                            show: true,
                                            rendererOptions: {
                                                numberRows: 1
                                            },
                                            location: 's'
                                        }
                                    });
             }

            });
        });  
        
        //remaining messages
        var x=[];
        var y=[];
        $.ajax({
            type:'POST',
            url:'<?php echo site_url('Admin/getBarData'); ?>',
            data:{org :org},
            success:function(data){
                    var dt=data.split(",");
                    
                    for(var i=0;i<dt.length;i++){
                        
                        var dt2=dt[i].split("::");
                        
                        x.push(dt2[0]);
                        y.push(Number.parseFloat(dt2[1])/1000);
                        
                    }
                    
                    var plot3 = $.jqplot('chart1', [y], {
                            // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
                            title:'Remaining Messages',
                            animate: !$.jqplot.use_excanvas,
                            grid: {
                                drawBorder: false, 
                                drawGridlines: true,
                                background: '#ffffff',
                                shadow:false
                            },
                            seriesDefaults:{
                                renderer:$.jqplot.BarRenderer,
                                pointLabels: { show: true }
                            },
                            axes: {
                                xaxis: {
                                    renderer: $.jqplot.CategoryAxisRenderer,
                                    label:'Organizations',
                                    labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
                                    labelOptions: {
                                                fontFamily: 'Georgia',
                                                fontSize: '10pt'
                                              },
                                    ticks: x,
                                    tickRenderer: $.jqplot.CanvasAxisTickRenderer,
                                    tickOptions: {
                                        // labelPosition: 'middle',
                                        angle: -15,
                                        fontFamily: 'Georgia',
                                        fontSize: '10pt',
                                    }
                                },
                                yaxis: {
                                    renderer: $.jqplot.CategoryAxisRenderer,
                                    label:'Message Count (x1000)',
                                    labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
                                    labelOptions: {
                                                fontFamily: 'Georgia',
                                                fontSize: '10pt'
                                              },
                                    tickOptions: {
                                        // labelPosition: 'middle',
                                        angle: 15
                                    }
                                }
                            },
                            highlighter: { show: false }
                        });
             }

            });
            
        //pending files
        $.ajax({
            type:'POST',
            url:'<?php echo site_url('Admin/getPendingFileData'); ?>',
            data:{org :org},
            success:function(data){
                    $('div#files').empty();
                    $('div#files').html(data);
             }

            });
    });
    
</script>
<div class="row">
    <div class="col-12">
        <div class="form-group offset-3 col-3">
            <label class="sr-only" for="org"></label>
            <select name="org" id="org" class="form-control">
                <option value="">All Organizations</option>
                <?php foreach($organizations as $key=>$value){ ?>

                <option value="<?php echo $value->ID; ?>" <?php echo $org == $value->ID?"selected='selected'":""; ?>><?php echo $value->NAME; ?></option>

                    <?php } ?>
            </select>
        </div>
    </div>
    <div class="col-6" id="chart" style="height:400px;width:400px; "></div>
    <div class="col-6" id="chart1"></div>
    <div class="col-8" id="files"></div>
    <div class="col-3"></div>
</div>