<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
 
   $(document).ready(function(){
       
   });
</script>
<div id="divform">
    <?php 
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open('Admin/register_subscription/'.$id); 
                ?>
       
                <div class="form-group row">
                    <label for="org" class="col-4 control-label">Organization</label>
                    <div class="col-4">
                        <select name="org" id="org" class="form-control" required>
                            <option></option>
                            <?php foreach($organizations as $key=>$value){ ?>

                            <option value="<?php echo $value->ID; ?>" <?php echo set_select('org',$value->ID); ?>><?php echo $value->NAME; ?></option>

                                <?php } ?>
                        </select>
                        <?php echo form_error('org'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="messagecount" class="col-4 control-label">Message Count</label>
                    <div class="col-4">
                        <input type="number" class="form-control" name="messagecount" id="messagecount" placeholder="Message Count" value="<?php echo set_value('messagecount'); ?>" required/>
                        <?php echo form_error('messagecount'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="offset-3 col-xs-12 col-4 btn btn-link">
                        <button type="submit" class="btn btn-success">Register Subscription</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>
