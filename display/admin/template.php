<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/bootstrap.css">
        <link rel="stylesheet" href="<?php echo base_url();?>styles/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/custom.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/jquery-ui.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/jquery.animatedheadline.css"/>
        
        <link rel="shortcut icon" href="<?php echo base_url();?>img/title.ico" />
        <script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery.js" ></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>styles/js/bootstrap.js" ></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery-ui.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery.animatedheadline.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>styles/js/custom.js"></script>
        
        <title>SMS</title>
        <script type="text/javascript">
            
            $(document).ready(function(){
                $('#banner').animatedHeadline();
                $('#banner').animatedHeadline({
                    animationType: "loading-bar",
                    animationDelay: 2500,
                    barAnimationDelay: 3800,
                    barWaiting: 800,
                    lettersDelay: 50,
                    typeLettersDelay: 150,
                    selectionDuration: 500,
                    typeAnimationDelay: 1300,
                    revealDuration: 600,
                    revealAnimationDelay: 1500
                });
            
            setInterval(function(){ 
                            var org='<?php echo $this->session->userdata('org'); ?>'
                            $.ajax({
                            type:'POST',
                            url:'<?php echo site_url('Admin/getSMSBalance'); ?>',
                            data:{org :org},
                            success:function(data){
                                    
                                    $('a#smsbalance').empty();
                                    $('a#smsbalance').append(data);
                             }

                            });
                        }, 300000);//time in milliseconds 
            });
            
        </script>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row" style="padding-bottom: 20px">
                <div class="col-2">
                    <img src="<?php echo base_url()?>img/logo.jpg" style="border-radius: 5px;height:100px;width: 200px"/>
                </div>
                <div class="col-7" id="banner">
                    <h1 class="ah-headline">
                        <span></span>
                        <span class="ah-words-wrapper">
                          <b class="is-visible">G-MOBILE BULK SMS</b>
                          <b>G-MOBILE BULK SMS</b>
                          <b>G-MOBILE BULK SMS</b>
                          <b>G-MOBILE BULK SMS</b>
                          <b>G-MOBILE BULK SMS</b>
                        </span>
                    </h1>
                </div>
                <div class="col-3">
                    <ul class="nav">
                        <li class="nav-item">
                            <span class="nav-link" style="font-style: italic;font-weight: bold">Balance&nbsp;:&nbsp;<a href="#" id="smsbalance"><?php echo $gatewayBalance; ?></a></span>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="<?php echo base_url()."index.php/Auth/logout"?>">Logout&nbsp;<i class="fa fa-sign-out fa-x1" aria-hidden="true"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-12" id="title"><?php echo $title; ?></div>
                <div class="col-2">
                    <?php foreach($panel as $val){ ?>
                    
                        <div class="userLinks">
                            <a class="btn btn-link" <?php echo $cls == $val['cls']?'id="activeLink"':''; ?> href="<?php echo base_url()."index.php/".$val['link']; ?>"><?php echo $val['title']; ?></a>
                        </div>
                    
                    <?php } ?>
                </div>
                <div class="col-10">
                    <div class="row">
                        <div class="col-8" id="responseMessage"><?php echo $message; ?></div>
                    </div>
                    
                    <?php $this->load->view($content); ?>
                </div>
            </div>
            <div class="row">
                <div class="col"></div>
                <div class="col" id="footer">G-Mobile Int&commat;<?php echo date('Y') ?>.All Rights Reserved</div>
            </div>
        </div>
    </body>
</html>