<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div  class="row">
    <div class="offset-4">
        <?php 
                    $attributes = array('class' => 'form-inline','role'=>'form');
                    echo form_open('Admin/senderids',$attributes); 
                ?>
                    <div class="form-group">
                        <label class="sr-only" for="sender"></label>
                        <input type="text" class="form-control" name="sender" id="sender" placeholder="Sender ID" value="<?php echo $sender; ?>" />
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="status"></label>
                        <select name="org" id="org" class="form-control">
                            <option value="">Organization</option>
                            <?php foreach($organizations as $key=>$value){ ?>

                            <option value="<?php echo $value->ID; ?>" <?php echo $org == $value->ID?"selected='selected'":""; ?>><?php echo $value->NAME; ?></option>

                                <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                       <input type="submit" class="btn btn-success" name="search"value="Search" /> 
                    </div>
        
            <?php echo form_close(); ?>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <table class="table table-condensed table-hover table-bordered">

        <thead>
            <tr>
                <th colspan="6" style="text-align:center;"><?php echo anchor('Admin/register_senderid','Register Sender ID'); ?></th>
             </tr>
            <tr>
                <th style="text-align:center;width:50px">S/NO</th>
                <th style="text-align:center;width:550px">Organization</th>
                <th style="text-align:center;width:200px">Sender ID</th>
                <th style="text-align:center;width:200px">Sender Code</th>
                <th style="text-align:center;width:250px">Status</th>
                <th style="text-align:center;width:250px">Action</th>
             </tr>
        </thead>
        <tbody>
            <?php if($data != null){
                
                if($per_page == null){
                        $i=1;
                    }else{
                        $i=$per_page+1;
                    }
                foreach($data as $key=>$value){ ?>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->NAME; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->SENDER; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->CODE; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->STATUS; ?></td>
                         <?php 
                        
                         if($value->STATUS === 'Active'){
                             
                             $active_status='<span class="btn btn-danger btn-sm" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Suspend">Suspend</span>';
                             
                         }else if($value->STATUS === 'Suspended'){
                             
                             $active_status='<span class="btn btn-success btn-sm" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Activate">Activate</span>';
                         }else{
                             
                             $active_status='<span class="btn btn-info btn-sm" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Approve">Approve</span>'; 
                        
                         }
                        
                         
                        ?>
                        <td>&nbsp;&nbsp;
                            <?php echo anchor('Admin/register_senderid/'.$value->ID,'<span class="fa fa-pencil-square-o fa-1x" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Edit"></span>'); ?>
                            &nbsp;&nbsp;
                            <?php echo anchor('Admin/activate_deactivate_senderid/'.$value->ID.'/'.$value->STATUS,$active_status); ?></td>
                    </tr>  
                <?php }
                }else{ ?>
            <tr>
                <td colspan="5" style="text-align:center"> NO DATA FOUND</td>
            </tr>  
                <?php } ?>
        </tbody>
    </table>
</div>
<div class="offset-4 col-4">
    <?php echo $links; ?>
</div>
</div>
