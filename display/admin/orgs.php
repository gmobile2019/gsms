<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div  class="row">
    <div class="offset-4">
        <?php 
                    $attributes = array('class' => 'form-inline','role'=>'form');
                    echo form_open('Admin/organizations',$attributes); 
                ?>
                    <div class="form-group">
                        <label class="sr-only" for="name"></label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="Organization Name" value="<?php echo $name; ?>" />
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="status"></label>
                        <select class="form-control" name="status" id="status">
                            <option value="">Status</option>
                            <option value="Active" <?php echo $status == "Active"?"selected='selected'":"" ?>>Active</option>
                            <option value="Suspended" <?php echo $status == "Suspended"?"selected='selected'":"" ?>>Suspended</option>
                        </select>
                    </div>
                    <div class="form-group">
                       <input type="submit" class="btn btn-success" name="search"value="Search" /> 
                    </div>
        
            <?php echo form_close(); ?>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <table class="table table-condensed table-hover table-bordered">

        <thead>
            <tr>
                <th colspan="6" style="text-align:center;"><?php echo anchor('Admin/register_org','Register Organization'); ?></th>
             </tr>
            <tr>
                <th style="text-align:center;width:50px">S/NO</th>
                <th style="text-align:center;width:550px">Name</th>
                <th style="text-align:center;width:200px">Address</th>
                <th style="text-align:center;width:250px">Phone</th>
                <th style="text-align:center;width:250px">Balance</th>
                <th style="text-align:center;width:200px">Action</th>
             </tr>
        </thead>
        <tbody>
            <?php if($data != null){
                
                if($per_page == null){
                        $i=1;
                    }else{
                        $i=$per_page+1;
                    }
                foreach($data as $key=>$value){ ?>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                        <td>&nbsp;&nbsp;<?php echo anchor('Admin/accountDetails/'.$value->ID,$value->NAME); ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->ADDRESS; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->PHONE; ?></td>
                        <td style="text-align: right"><?php echo number_format($this->Administrator_model->messageBalance($value->ID)->REMAININGCOUNT); ?>&nbsp;&nbsp;</td>
                        <?php 
                        
                        $active_status=$value->STATUS == 'Active'?'<span class="btn btn-danger btn-sm" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Deactivate">Suspend</span>':'<span class="btn btn-success btn-sm" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Activate">Activate</span>'; 
                        
                        ?>
                        <td>&nbsp;&nbsp;
                            <?php echo anchor('Admin/register_org/'.$value->ID,'<span class="fa fa-pencil-square-o fa-1x" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Edit"></span>'); ?>
                            &nbsp;&nbsp;
                            <?php echo anchor('Admin/activate_deactivate_org/'.$value->ID.'/'.$value->STATUS,$active_status); ?></td>
                    </tr>  
                <?php }
                }else{ ?>
            <tr>
                <td colspan="6" style="text-align:center"> NO DATA FOUND</td>
            </tr>  
                <?php } ?>
        </tbody>
    </table>
    <div align="center">
        <?php echo $links; ?>
    </div>
</div>
</div>
