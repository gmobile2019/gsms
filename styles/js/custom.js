$(document).ready(function(){
   
    var msgCategory;
    var bulk;
    var now;
    var scheduleNo;
    var recipientNo;
    var variableNo;
    var recipients;
    var schedules;
    
    $('div#recipients').hide();
    $('div#schedules').hide();
    $('div#fewPersonalized').hide();
    
    msgCategory=$('select#msgcategory').val();
    bulk=$('select#bulk').val();
    now=$('select#now').val();
    recipientNo=$('select#recipientsNo').val();
    variableNo=$('select#variableNo').val();
    scheduleNo=$('select#scheduleNo').val();
    
    if(bulk === "Yes"){
        if(msgCategory ==='Personalized'){
            
            $('span#personalized').show();
            $('span#general').hide();
        }else{

            $('span#personalized').hide();
            $('span#general').show();
        }
    }
    //message category control on compose message page load
    if(msgCategory ==='Personalized'){
        $('div#fewPersonalized').show();
    }
    
    //message category change monitor event
    $('select#msgcategory').change(function(){
        
        msgCategory=$(this).val();
        if(bulk === "Yes"){
        
            $('div.bulkUpload').show();
            $('div.fewUpload').hide();
            
            if(msgCategory ==='Personalized'){
            
                $('span#personalized').show('slow');
                $('span#general').hide('slow');
            }else{

                $('span#personalized').hide('slow');
                $('span#general').show('slow');
            }
            
        }else{
            
            $('div.bulkUpload').hide();
            $('div.fewUpload').show();
            
            if(msgCategory ==='Personalized'){
            
                $('div#fewPersonalized').show('slow');
                

            }else{
                
                $('div#fewPersonalized').hide('slow');
            }
            
            recipients ='';    
            for(var i=1;i<=recipientNo;i++){
                recipients +='<div class="offset-4 col-3"><input type="tel" pattern="^\+?\d{0,13}" class="form-control" name="recipient'+i+'" id="recipient'+i+'" placeholder="Recipient '+i+'" /></div>';
                if(msgCategory === "Personalized"){
                    recipients +='<div class="col-3"><ul style="list-style:none">';
                    for(var j=1;j<=variableNo;j++){
                        recipients +='<li><input type="text" class="form-control" name="var'+i+'[]" id="var'+i+'[]" placeholder="Variable '+j+' (recipient '+i+')" /></li>';
                    }
                    recipients +='</div></ul>';
                }

            }

            $('div#recipients').empty();
            $('div#recipients').html(recipients);
            $('div#recipients').show('slow');
            }
        
        
    });
    
    //bulk upload control on compose message page load
    if(bulk === "Yes"){
        
        $('div.bulkUpload').show();
        $('div.fewUpload').hide();
    }else{
        
        $('div.bulkUpload').hide();
        $('div.fewUpload').show();
        if(msgCategory === "General"){
            $('div#fewPersonalized').hide();
        }
        
        recipients ='';    
        for(var i=1;i<=recipientNo;i++){
            recipients +='<div class="offset-4 col-3"><input type="tel" pattern="^\+?\d{0,13}" class="form-control" name="recipient'+i+'" id="recipient'+i+'" placeholder="Recipient '+i+'" /></div>';
            if(msgCategory === "Personalized"){
                recipients +='<div class="col-3"><ul style="list-style:none">';
                for(var j=1;j<=variableNo;j++){
                    recipients +='<li><input type="text" class="form-control" name="var'+i+'[]" id="var'+i+'[]" placeholder="Variable '+j+' (recipient '+i+')" /></li>';
                }
                recipients +='</div></ul>';
            }

        }

        $('div#recipients').empty();
        $('div#recipients').html(recipients);
        $('div#recipients').show('slow');
    }
    
    //changing bulk upload event monitor
    $('select#bulk').change(function(){
        
        bulk=$(this).val();
        if(bulk === "Yes"){
        
            $('div.bulkUpload').show('slow');
            $('div.fewUpload').hide('slow');
        }else{

            $('div.bulkUpload').hide('slow');
            $('div.fewUpload').show('slow');
            if(msgCategory === "General"){
                $('div#fewPersonalized').hide();
            }
            recipients='';
            for(var i=1;i<=recipientNo;i++){
                recipients +='<div class="offset-4 col-3"><input type="text" class="form-control" name="recipient'+i+'" id="recipient'+i+'" placeholder="Recipient '+i+'" /></div>';
                if(msgCategory === "Personalized"){
                    recipients +='<div class="col-3"><ul style="list-style:none">';
                    for(var j=1;j<=variableNo;j++){
                        recipients +='<li><input type="text" class="form-control" name="var'+i+'[]" id="var'+i+'[]" placeholder="Variable '+j+' (recipient '+i+')" /></li>';
                    }
                    recipients +='</div></ul>';
                }

            }
            $('div#recipients').empty();
            $('div#recipients').html(recipients);
            $('div#recipients').show('slow');
            
        }
    });
    
    //changing recipient count event monitor
    $('select#recipientsNo').change(function(){
        recipientNo=$(this).val();
        recipients ='';
   
        for(var i=1;i<=recipientNo;i++){
            recipients +='<div class="offset-4 col-3"><input type="tel" pattern="^\+?\d{0,13}" class="form-control" name="recipient'+i+'" id="recipient'+i+'" placeholder="Recipient '+i+'" /></div>';
            if(msgCategory === "Personalized"){
                recipients +='<div class="col-3"><ul style="list-style:none">';
                for(var j=1;j<=variableNo;j++){
                    recipients +='<li><input type="text" class="form-control" name="var'+i+'[]" id="var'+i+'[]" placeholder="Variable '+j+' (recipient '+i+')" /></li>';
                }
                recipients +='</div></ul>';
            }

        }
        
        $('div#recipients').empty();
        $('div#recipients').html(recipients);
        $('div#recipients').show('slow');
    });
    
    //changing variable count event monitor
    $('select#variableNo').change(function(){
        variableNo=$(this).val();
        recipients ='';
   
        for(var i=1;i<=recipientNo;i++){
            recipients +='<div class="offset-4 col-3"><input type="tel" pattern="^\+?\d{0,13}" class="form-control" name="recipient'+i+'" id="recipient'+i+'" placeholder="Recipient '+i+'" /></div>';
            if(msgCategory === "Personalized"){
                recipients +='<div class="col-3"><ul style="list-style:none">';
                for(var j=1;j<=variableNo;j++){
                    recipients +='<li><input type="text" class="form-control" name="var'+i+'[]" id="var'+i+'[]" placeholder="Variable '+j+' (recipient '+i+')" /></li>';
                }
                recipients +='</div></ul>';
            }

        }
        
        $('div#recipients').empty();
        $('div#recipients').html(recipients);
        $('div#recipients').show('slow');
    });
    
    //sending schedule control on compose message page load
    if(now === "Yes"){
        
        $('div.scheduleSending').hide();
        
    }else{
        
        $('div.scheduleSending').show();
        
        schedules ='';
        for(var i=1;i<=scheduleNo;i++){
            
            schedules +='<div class="offset-4 col-3"><input type="date" class="form-control" name="schedule'+i+'" id="schedule'+i+'" placeholder="Schedule '+i+'" /></div>';
            schedules +='<div class="col-3"><input type="text" class="form-control" name="time'+i+'" id="time'+i+'" placeholder="hh:mm:ss" /></div>';
            
        }
        
        $('div#schedules').empty();
        $('div#schedules').html(schedules);
        $('div#schedules').show('slow');
    }
    
    //changing sending schedule count event monitor
    $('select#now').change(function(){
        now=$(this).val();
        if(now === "Yes"){
        
            $('div.scheduleSending').hide();

        }else{

            $('div.scheduleSending').show();

            schedules ='';
            for(var i=1;i<=scheduleNo;i++){
            
                schedules +='<div class="offset-4 col-3"><input type="date" class="form-control" name="schedule'+i+'" id="schedule'+i+'" placeholder="Schedule '+i+'" /></div>';
                schedules +='<div class="col-3"><input type="text" class="form-control" name="time'+i+'" id="time'+i+'" placeholder="hh:mm:ss" /></div>';

            }

            $('div#schedules').empty();
            $('div#schedules').html(schedules);
            $('div#schedules').show('slow');
        }
    });
    
    //changing schedule count event monitor
    $('select#scheduleNo').change(function(){
        scheduleNo=$(this).val();
        schedules ='';
        for(var i=1;i<=scheduleNo;i++){

            schedules +='<div class="offset-4 col-3"><input type="date" class="form-control" name="schedule'+i+'" id="schedule'+i+'" placeholder="Schedule '+i+'" /></div>';
            schedules +='<div class="col-3"><input type="text" class="form-control" name="time'+i+'" id="time'+i+'" placeholder="hh:mm:ss" /></div>';

        }

        $('div#schedules').empty();
        $('div#schedules').html(schedules);
        $('div#schedules').show('slow');
    });
    
    //SMS count control on compose message page load
    var msgCount="("+Math.ceil(($('textarea#message').val().length)/160)+")";
    $('span#smsCount').empty();
    $('span#smsCount').text(msgCount);
    
    //content area event monitor
    $('textarea#message').keyup(function(){
       
        var msgCount="("+Math.ceil(($(this).val().length)/160)+")";
        $('span#smsCount').empty();
        $('span#smsCount').text(msgCount);
    });
});


