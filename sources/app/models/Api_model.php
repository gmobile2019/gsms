<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Api_model
 *
 * @author Manuel
 */
class Api_model extends CI_Model{
    
    private $smsgateway;
    
    public function __construct()
    {
            parent::__construct();
            $this->smsgateway=$this->load->database('smsgateway',TRUE);//load smsgateway database configuration
    }
    
    function check_access_credentials($token,$username,$password){
        
        $data=$this->smsgateway->query("SELECT id FROM api_credentials "
                                        . "WHERE token='$token' AND username='$username' AND password='$password' AND status='1'")->row();
        
        if($data <> null){
            
            return TRUE;
        }
        
        return FALSE;
    }
    
    function sender($code){
        
        
        return $this->smsgateway->query("SELECT ID,ORGID,SENDER,CODE "
                                        . "FROM sms_sender_id "
                                        . "WHERE CODE='$code' AND STATUS='Active' ")->row();
    }
    
    function get_response_details($errorcode){
        
        return $this->smsgateway->query("SELECT id,code,status,description FROM api_response_codes WHERE code='$errorcode'")->row();
    }
}