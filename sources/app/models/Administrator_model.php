<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Administrator_model extends CI_Model
{
   
    private $smsgateway;
    
    public function __construct()
    {
            parent::__construct();
            $this->smsgateway=$this->load->database('smsgateway',TRUE);//load smsgateway database configuration
            $this->store_salt      = $this->config->item('store_salt', 'ion_auth');
            $this->salt_length     = $this->config->item('salt_length', 'ion_auth');
    }
    
    function sender_ids_count($company,$sender){
        
        if($sender <> null){
            
           $where .=" AND sender.SENDER LIKE '%$sender%'"; 
        }
       
        
        if($company <> null){
            
            $where .=" AND sender.ORGID='$company'";
        }
        
        
        return count($this->smsgateway->query("SELECT sender.ID,sender.ORGID,sender.SENDER,sender.CODE,"
                . "sender.STATUS,company.NAME "
                . "FROM sms_sender_id as sender "
                . "INNER JOIN companies AS company "
                . "ON sender.ORGID=company.ID "
                . "$where "
                . "ORDER BY company.NAME,sender.SENDER ASC")->result());
    }
    
    function sender_ids($company,$sender,$page,$limit){
       
       if($sender <> null){
            
           $where .=" AND sender.SENDER LIKE '%$sender%'"; 
        }
       
        
        if($company <> null){
            
            $where .=" AND sender.ORGID='$company'";
        }
        
        return $this->smsgateway->query("SELECT sender.ID,sender.ORGID,sender.SENDER,sender.CODE,"
                . "sender.STATUS,company.NAME "
                . "FROM sms_sender_id as sender "
                . "INNER JOIN companies AS company "
                . "ON sender.ORGID=company.ID "
                . "$where "
                . "ORDER BY company.NAME,sender.SENDER ASC "
                . "LIMIT $page,$limit")->result();
    }
    
    function create_sender_id($array,$id){
        if($id == null){
          $array['CREATEDBY']=$this->session->userdata('user_id');
          $array['CREATEDON']=date('Y-m-d H:i:s');
          if(!$this->smsgateway->insert('sms_sender_id',$array)){
              
              return FALSE;
          }
          
          $id=$this->smsgateway->insert_id();
          
          $code=str_pad($id,5,'0',STR_PAD_RIGHT);
          $this->smsgateway->update('sms_sender_id',array('CODE'=>$code),array('ID'=>$id));
          
          return TRUE;
       } 
       
        $array['MODIFIEDBY']=$this->session->userdata('user_id');
        $array['MODIFIEDON']=date('Y-m-d H:i:s');
        return $this->smsgateway->update('sms_sender_id',$array,array('ID'=>$id));
    }
    
    function senders($company,$id,$status){
        
        if($company <> null){
            
            $where .=" AND sender.ORGID='$company'";
        }
        
        if($id <> null){
            
            $where .=" AND sender.ID='$id'";
        }
        
        if($status <> null){
            
            $where .=" AND sender.STATUS='$status'";
        }
        
        return $this->smsgateway->query("SELECT sender.ID,sender.ORGID,sender.SENDER,sender.CODE,"
                . "sender.STATUS,company.NAME "
                . "FROM sms_sender_id as sender "
                . "INNER JOIN companies AS company "
                . "ON sender.ORGID=company.ID "
                . "$where "
                . "ORDER BY company.NAME,sender.SENDER ASC")->result();
    }
    
    function activate_deactivate_senderid($id,$status){
        $new_status=$status == 'Active'?'Suspended':'Active';
        
        return $this->smsgateway->update('sms_sender_id',array('STATUS'=>$new_status),array('ID'=>$id));
    }
    
    public function save_accountDetails($account,$key){
        
        return $this->smsgateway->update('users',$account,array('ID'=>$key));
    }
    
    function access_credentials_count($company){
        
        if($company <> null){
            
            $where .=" AND api.companyid='$company'";
        }
        
        
        return count($this->smsgateway->query("SELECT api.id,api.companyid,api.token,api.username,api.password,"
                . "api.status,company.NAME "
                . "FROM api_credentials as api "
                . "INNER JOIN companies AS company "
                . "ON api.companyid=company.ID "
                . "$where "
                . "ORDER BY company.NAME ASC")->result());
    }
    
    function access_credentials($company,$page,$limit){
       
       if($company <> null){
            
            $where .=" AND api.companyid='$company'";
        }
        
        return $this->smsgateway->query("SELECT api.id,api.companyid,api.token,api.username,api.password,"
                . "api.status,company.NAME "
                . "FROM api_credentials as api "
                . "INNER JOIN companies AS company "
                . "ON api.companyid=company.ID "
                . "$where "
                . "ORDER BY company.NAME ASC "
                . "LIMIT $page,$limit")->result();
    }
    
    function profile_data(){
        
        return $this->smsgateway->query("SELECT users.SALT,users.PASSWORD,users.ID,users.FULLNAME,users.COMPANY,users.USERNAME,users_groups.GROUP_ID FROM users INNER JOIN users_groups ON users.ID=users_groups.USER_ID WHERE users.ID='".$this->session->userdata('user_id')."'")->row();
    }
    
    function system_user_registration($user,$user_group,$id,$password){
        
       if($password <> null){
           $salt= $this->store_salt ? $this->salt() : FALSE;
       
            if(!$salt){

                return FALSE;
            }

            $password= $this->hash_password($password, $salt);
            
            $user['PASSWORD']=$password;
            $user['SALT']=$salt;
       }
       
      if($id == null){
          
          $user['CREATEDON']=date('YmdHis');
          $user['CREATEDBY']=$this->session->userdata('user_id');
          $usr=$this->smsgateway->insert('users',$user);
          
          if($user){
             
              $user_group['USER_ID']=$this->smsgateway->insert_id();
              return $this->smsgateway->insert('users_groups',$user_group);
          }
          return FALSE;
      } 
              $user['MODIFIEDON']=date('YmdHis');
              $user['MODIFIEDBY']=$this->session->userdata('user_id');
          
              $this->smsgateway->update('users',$user,array('ID'=>$id));
       return $this->smsgateway->update('users_groups',$user_group,array('USER_ID'=>$id));
    }
    
    function change_password($password){
        
        $salt=$this->profile_data();
        $password=$this->hash_password($password,$salt->SALT);
        
        return $this->smsgateway->update('users',array('PASSWORD'=>$password),array('ID'=>$this->session->userdata('user_id')));
    }
    
    function salt()
    {
        
            return substr(md5(uniqid(rand(), true)), 0,$this->salt_length);
    }
    
    function hash_password($password, $salt=false)
	{
		if ($this->store_salt && $salt)
		{
			return md5($password.$salt);
		}
		else
		{
			$salt = $this->salt();
			return  $salt . substr(md5($salt . $password), 0, -$this->salt_length);
		}
	}
    
    function companies($id,$status){
        
        if($id <> null){
            
            $where .=" AND ID='$id'";
            
        }
        
        if($status <> null){
            
            $where .=" AND STATUS='$status'";
            
        }
        
        return $this->smsgateway->query("SELECT ID,NAME,ADDRESS,PHONE,EMAIL,USERNAME,APIPSS,USERPSS,TOKEN,STATUS FROM companies WHERE ID is not null $where ORDER BY NAME ASC")->result();
    }
    
    function registered_companies_count($name,$status){
        
        if($name <> null){
            
           $where .=" AND NAME LIKE '%$name%'"; 
        }
        
        if($status <> null){
            
           $where .=" AND STATUS LIKE '$status'"; 
        }
        
        return count($this->smsgateway->query("SELECT ID,NAME,"
                . "ADDRESS,PHONE,EMAIL,USERNAME,APIPSS,USERPSS,TOKEN,STATUS "
                . "FROM companies WHERE ID is not null $where")->result());
    }
    
    function registered_companies($name,$status,$page,$limit){
        
       if($name <> null){
            
           $where .=" AND NAME LIKE '%$name%'"; 
        }
        
        if($status <> null){
            
           $where .=" AND STATUS LIKE '$status'"; 
        }
        
        return $this->smsgateway->query("SELECT ID,NAME,"
                . "ADDRESS,PHONE,EMAIL,USERNAME,APIPSS,USERPSS,TOKEN,STATUS "
                . "FROM companies WHERE ID is not null $where"
                . "ORDER BY NAME ASC "
                . "LIMIT $page,$limit")->result();
    }
    
    function groups($id){
        
        if($id <> null){
            
            $where =" WHERE ID='$id'";
            
        }
        
        return $this->smsgateway->query("SELECT ID,NAME,DESCRIPTION FROM groups $where ORDER BY NAME ASC")->result();
    }
    
    function member($username,$company){
        
        if($username <> null){
            
           $where .=" AND USERNAME='$username'"; 
        }
        
        if($company <> null){
            
            $where .=" AND COMPANY='$company'"; 
        }
        
        return $this->smsgateway->query("SELECT ID,FULLNAME,USERNAME,STATUS,COMPANY FROM users WHERE ID is not NULL $where")->result();
    }
    
    function activate_deactivate_users($id,$status){
        $new_status=$status == 1?0:1;
        
        return $this->smsgateway->update('users',array('STATUS'=>$new_status),array('ID'=>$id));
    }
    
    function activate_deactivate_companies($id,$status){
        $new_status=$status == 'Active'?'Suspended':'Active';
        
               $this->smsgateway->update('users',array('STATUS'=>$new_status),array('COMPANY'=>$id));
               $this->smsgateway->update('message_balance',array('STATUS'=>$new_status),array('ORGID'=>$id));
        return $this->smsgateway->update('companies',array('STATUS'=>$new_status),array('ID'=>$id));
    }
    
    function register_company($company,$id){
       
        if($id == null){
          
          $company['CREATED_ON']=date('Y-m-d H:i:s');
          $company['CREATED_BY']=$this->session->userdata('user_id');
          $this->smsgateway->insert('companies',$company);
          $ins_id=$this->smsgateway->insert_id();
          
          if($ins_id){
              
            //create message account
            $array=array(
                'ORGID'=>$ins_id,
                'REMAININGCOUNT'=>0,
                'LASTUPDATE'=>date('Y-m-d H:i:s')
            );
            
            $cmpy=array(
                 'USERNAME'=>$this->getUsername(),
                 'APIPSS'=>$this->getApiPass(),
                 'USERPSS'=>$this->getUserPass(),
                 'TOKEN'=>$this->getApitoken(),
             );
           
            $api=array(
                'companyid'=>$ins_id,
                'token'=>$cmpy['TOKEN'],
                'username'=>$cmpy['USERNAME'],
                'password'=>$this->hash_api_password($cmpy['APIPSS']),
                'createdon'=>date('Y-m-d H:i:s'),
            );
           
            $user=array(
                       'FULLNAME'=>$company['NAME'],
                       'USERNAME'=>$cmpy['USERNAME'],
                       'COMPANY'=>$ins_id,
                       'CREATEDON'=>date('Y-m-d H:i:s'),
                   );
            
            $smsBatch=array(
                'COMPANY'=>$ins_id,
                'NEXT'=>1,
                'LASTUPDATE'=>date('Y-m-d H:i:s')
            );
            
            $this->smsgateway->update('companies',$cmpy,array('ID'=>$ins_id));
            $this->smsgateway->insert('message_balance',$array);
            $this->smsgateway->insert('api_credentials',$api);
            $this->smsgateway->insert('sms_batches_control',$smsBatch);
            $this->system_user_registration($user,array('GROUP_ID'=>$this->config->item('portal_user_grp')),NULL,$cmpy['USERPSS']);
            return TRUE;
          }
          
         return FALSE;
      } 
      
        
        $company['MODIFIED_ON']=date('Y-m-d H:i:s');
        $company['MODIFIED_BY']=$this->session->userdata('user_id');
         
        return  $this->smsgateway->update('companies',$company,array('ID'=>$id));
    }
    
    function getUsername(){
        
        while(true){
            $username=$this->config->item('keyWord').'_'.substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ"),-3);
            $user=$this->member($username);
            
            if($user == NULL){
                
                return $username;
            }
        }
    }
    
    function getUserPass(){
        
        return substr(str_shuffle("abcdefghijklmnopqrstuvwxyz123456789!@#$*?-_"),-6);
    }
    
    function getApiPass(){
        
        return substr(str_shuffle("abcdefghijklmnopqrstuvwxyz123456789!@#$*?-_"),-8);
    }
    
    function getApitoken(){
        
        return substr(str_shuffle($this->config->item('source_token_string')),0,50);
    }
    
    function hash_api_password($password){
        
        $length=strlen($password)-$this->config->item('offset');
        
        return md5($password.substr($this->config->item('salt'),$this->config->item('offset'), $length));
    }
    
    function save_subscription($subscription){
       
          $insert=$this->smsgateway->insert('message_subscriptions',$subscription);
          $insertid=$this->smsgateway->insert_id();
          
        if($insert){
              
            $subscriptionid=  str_pad($insertid,6, 0,STR_PAD_LEFT);
            $this->smsgateway->update('message_subscriptions',array('SUBSCRIPTIONID'=>$subscriptionid),array('ID'=>$insertid));
              
            return $this->smsgateway->query("UPDATE message_balance "
                                            . "SET REMAININGCOUNT=(REMAININGCOUNT + ".$subscription['MESSAGECOUNT']."),LASTUPDATE='".date('Y-m-d H:i:s')."' "
                                            . "WHERE ORGID='".$subscription['ORGID']."'");
        }
        return FALSE;
    }
    
    function subscriptions_count($org,$start,$end){
        
       if($org <> null){
            
            $where .=" AND s.ORGID='$org'";
            
        }
        
        if($start <> null){
            
            $where .=" AND s.LASTUPDATE >='$start 00:00:00'";
            
        }
        
        if($end <> null){
            
            $where .=" AND s.LASTUPDATE <='$end 23:59:59'";
            
        }
        
        return count($this->smsgateway->query("SELECT s.ID,c.NAME "
                                . "FROM message_subscriptions AS s "
                                . "INNER JOIN companies as c "
                                . "ON s.ORGID=c.ID "
                                . "WHERE s.ID is not null $where")->result());
    }
    
    function subscriptions_info($org,$start,$end,$page,$limit){
        
        if($org <> null){
            
            $where .=" AND s.ORGID='$org'";
            
        }
        
        if($start <> null){
            
            $where .=" AND s.LASTUPDATE >='$start 00:00:00'";
            
        }
        
        if($end <> null){
            
            $where .=" AND s.LASTUPDATE <='$end 23:59:59'";
            
        }
        
        return $this->smsgateway->query("SELECT s.ID,s.ORGID,DATE_FORMAT(s.LASTUPDATE,'%d/%m/%Y') AS LASTUPDATE,"
                                . "s.STATUS,s.USERID,s.MESSAGECOUNT,s.SUBSCRIPTIONID,c.NAME "
                                . "FROM message_subscriptions AS s "
                                . "INNER JOIN companies as c "
                                . "ON s.ORGID=c.ID "
                                . "WHERE s.ID is not null $where ORDER BY s.ID DESC "
                                . "LIMIT $page,$limit")->result();
    }
    
    function subscriptions($id,$org,$start,$end,$status){
        
        if($id <> null){
            
            $where .=" AND S.ID='$id'";
            
        }
        
        if($org <> null){
            
            $where .=" AND s.ORGID='$org'";
            
        }
        
        if($start <> null){
            
            $where .=" AND s.LASTUPDATE >='$start 00:00:00'";
            
        }
        
        if($end <> null){
            
            $where .=" AND s.LASTUPDATE <='$end 23:59:59'";
            
        }
        
        if($status <> null){
            
             $where .=" AND S.STATUS='$status'";
        }
        
        return $this->smsgateway->query("SELECT s.ID,s.ORGID,DATE_FORMAT(s.LASTUPDATE,'%d/%m/%Y') AS LASTUPDATE,"
                                . "s.STATUS,s.USERID,s.MESSAGECOUNT,s.SUBSCRIPTIONID,c.NAME "
                                . "FROM message_subscriptions AS s "
                                . "INNER JOIN companies as c "
                                . "ON s.ORGID=c.ID "
                                . "WHERE s.ID is not null $where ORDER BY s.ID DESC")->result();
    }
    
    function messageBalance($org){
        
        return $this->smsgateway->query("SELECT mb.ID,mb.ORGID,DATE_FORMAT(mb.LASTUPDATE,'%d/%m/%Y') AS LASTUPDATE,"
                                . "mb.REMAININGCOUNT,c.NAME "
                                . "FROM message_balance AS mb "
                                . "INNER JOIN companies as c "
                                . "ON mb.ORGID=c.ID "
                                . "WHERE mb.ORGID='$org'")->row();
    }
    
    function messageStatus(){
        
        return $this->smsgateway->query("SELECT ID,STATUS,DESCRIPTION,INFOBIPGROUPID FROM sms_status")->result(); 
    }
    
    function messages_count($org,$from_schedule,$to_schedule,$status,$msisdn){
        
        if($msisdn <> null){
            
            $where .=" AND MSISDN LIKE '%$msisdn%'";
        }
        
        if($from_schedule <> null){
            
            $where .=" AND SCHEDULETIME>= '$from_schedule 00:00:00'";
        }
        
        if($to_schedule <> null){
            
            $where .=" AND SCHEDULETIME <= '$to_schedule 23:59:59'";
        }
        
        if($status <> null){
            
            $where .=" AND STATUS LIKE '$status'";
        }
        
        if($org <> null){
            
            $where .=" AND ORGID='$org'";
        }
        
        $count=$this->smsgateway->query("SELECT COUNT(ID) AS id_count FROM sms_txns WHERE ID is not null $where")->row();
        
        return $count->id_count;
    }
    
    function messages_info($org,$from_schedule,$to_schedule,$status,$msisdn,$page,$limit){
        
        
        if($msisdn <> null){
            
            $where .=" AND s.MSISDN LIKE '%$msisdn%'";
        }
        
        if($from_schedule <> null){
            
            $where .=" AND s.SCHEDULETIME>= '$from_schedule 00:00:00'";
        }
        
        if($to_schedule <> null){
            
            $where .=" AND s.SCHEDULETIME <= '$to_schedule 23:59:59'";
        }
        
        if($status <> null){
            
            $where .=" AND s.STATUS LIKE '$status'";
        }
        
        if($org <> null){
            
            $where .=" AND s.ORGID='$org'";
        }
        
         return $this->smsgateway->query("SELECT s.ID,s.SMS,s.SENDERID,s.ORGID,DATE_FORMAT(s.SCHEDULETIME,'%d/%m/%Y %l:%i %p') AS SCHEDULETIME,"
                 . "s.MSISDN,s.STATUS,s.MESSAGECOUNT,s.MESSAGEID,c.NAME "
                 . "FROM sms_txns AS s "
                 . "INNER JOIN companies as c "
                 . "ON s.ORGID=c.ID "
                 . "WHERE s.ID is not null $where "
                 . "ORDER BY s.SCHEDULETIME DESC "
                 . "LIMIT $page,$limit")->result();
    }
    
    function messages($org,$from_schedule,$to_schedule,$status,$msisdn){
        
        
        if($msisdn <> null){
            
            $where .=" AND s.MSISDN LIKE '%$msisdn%'";
        }
        
        if($from_schedule <> null){
            
            $where .=" AND s.SCHEDULETIME>= '$from_schedule 00:00:00'";
        }
        
        if($to_schedule <> null){
            
            $where .=" AND s.SCHEDULETIME <= '$to_schedule 23:59:59'";
        }
        
        if($status <> null){
            
            $where .=" AND s.STATUS LIKE '$status'";
        }
        
        if($org <> null){
            
            $where .=" AND s.ORGID='$org'";
        }
        
         return $this->smsgateway->query("SELECT s.ID,s.SMS,s.SENDERID,s.ORGID,DATE_FORMAT(s.SCHEDULETIME,'%d/%m/%Y %l:%i %p') AS SCHEDULETIME,"
                 . "s.MSISDN,s.STATUS,s.MESSAGECOUNT,s.MESSAGEID,c.NAME "
                 . "FROM sms_txns AS s "
                 . "INNER JOIN companies as c "
                 . "ON s.ORGID=c.ID "
                 . "WHERE s.ID is not null $where "
                 . "ORDER BY s.SCHEDULETIME DESC")->result();
    }
   
    function save_file_data($data){
        
        return $this->smsgateway->insert('uploaded_files',$data);
    }
    
    function save_message_data($data){
        
        return $this->smsgateway->insert_batch('sms_txns',$data);
    }
    
    function save_message_data2($data){
        
        return $this->smsgateway->insert('sms_txns',$data);
    }
    
    function sms_batch($company){
        $fixed=99;
        $nxt=$this->smsgateway->get_where('sms_batches_control',array('COMPANY'=>$company))->row();
        
        $batch=$fixed.str_pad($nxt->NEXT, 8,0,STR_PAD_LEFT);
        
        return $batch;
    }
    
    function update_sms_batch($company){
        
        $this->smsgateway->query("UPDATE sms_batches_control SET NEXT=NEXT+1,LASTUPDATE='".date('Y-m-d H:i:s')."' WHERE COMPANY='$company'");
    }
    
    function reset_api_username_password($array,$id){
        
        return $this->smsgateway->update('api_credentials',$array,array('id'=>$id));
    }
    
    function reset_access_token($array,$id){
        
        return $this->smsgateway->update('api_credentials',$array,array('id'=>$id));
    }
    
    function activate_deactivate_api_access($id,$status){
        $new_status=$status == 1?0:1;
        
        return $this->smsgateway->update('api_credentials',array('status'=>$new_status),array('id'=>$id));
    }
    
    function account_details($org){
        
        $company=$this->companies($org);
        $balance=$this->messageBalance($org);
        $senders=array();
                
        foreach($this->senders($org,NULL, 'Active') as $key=>$value){
            
            $senders[]=$value->SENDER;
        }
        
        return array(
                    'orgName'=>$company[0]->NAME,
                    'orgAddress'=>$company[0]->ADDRESS,
                    'orgPhone'=>$company[0]->PHONE,
                    'orgEmail'=>$company[0]->EMAIL,
                    'messageBalance'=>$balance->REMAININGCOUNT,
                    'portalUsername'=>$company[0]->USERNAME,
                    'portalPss'=>$company[0]->USERPSS,
                    'apiUsername'=>$company[0]->USERNAME,
                    'apiPassword'=>$company[0]->APIPSS,
                    'apiToken'=>$company[0]->TOKEN,
                    'senders'=>$senders,
                ); 
    }
    
    function getPieData($org){
        
        if($org <> null){
            
            $where .=" AND ORGID='$org'";
        }
        
        return $this->smsgateway->query("SELECT COUNT(ID) AS smsCount,STATUS "
                                        . "FROM sms_txns "
                                        . "WHERE ID is not null $where "
                                        . "GROUP BY STATUS ORDER BY STATUS ASC")->result();
    }
    
    function getBarData($org){
        
        return $this->smsgateway->query("SELECT mb.REMAININGCOUNT,c.NAME "
                                        . "FROM message_balance AS mb "
                                        . "INNER JOIN companies AS c "
                                        . "ON mb.ORGID=c.ID "
                                        . "WHERE mb.ID is not null ORDER BY mb.REMAININGCOUNT ASC")->result();
    }
    
    function getPendingFileData($org){
        
        if($org <> null){
            
            $where .=" AND uf.orgid='$org'";
        }
        
        return $this->smsgateway->query("SELECT uf.filename,uf.org_filename,uf.scheduletime,"
                                        . "uf.senderid,uf.fext,c.NAME "
                                        . "FROM uploaded_files AS uf "
                                        . "INNER JOIN companies AS c "
                                        . "ON uf.orgid=c.ID "
                                        . "WHERE uf.status=2 ORDER BY c.NAME ASC")->result();
    }
    
    function report($startdate,$enddate,$org,$status){
        
        if($org <> null){
            
            $where .=" AND st.ORGID='$org'";
        }
        
        if($status <> null){
            
            $where .=" AND st.STATUS='$status'";
        }
        
        return $this->smsgateway->query("SELECT SUM(st.MESSAGECOUNT) AS smsCount,st.STATUS,c.NAME "
                                        . "FROM sms_txns AS st "
                                        . "INNER JOIN companies AS c "
                                        . "ON st.ORGID=c.ID "
                                        . "WHERE st.SCHEDULETIME>='$startdate 00:00:00' AND st.SCHEDULETIME <='$enddate 23:59:59' $where "
                                        . "GROUP BY c.NAME,st.STATUS "
                                        . "ORDER BY c.NAME,st.STATUS ASC")->result();
    }
}