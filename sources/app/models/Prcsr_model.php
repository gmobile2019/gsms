<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Prcsr_model extends CI_Model
{
   
    private $smsgateway;
    
    public function __construct()
    {
            parent::__construct();
            $this->smsgateway=$this->load->database('smsgateway',TRUE);//load smsgateway database configuration
    }
    
    function pending_files(){
        
        return $this->smsgateway->query("SELECT id,filename,scheduletime,message,"
                . "orgid,senderid,fext,"
                . "smstype,smsbatch,status,createdon,createdby "
                . "FROM uploaded_files "
                . "WHERE status=2 ORDER BY scheduletime ASC")->result();
    }
    
    function update_file_status($id){
        
        return $this->smsgateway->update('uploaded_files',array('status'=>1),array('id'=>$id));
    }
    
    function getSmsPendings($orgid){
        
        if($orgid <> NULL){
            
            $where .=" AND ORGID='$orgid'";
        }
        
        return $this->smsgateway->query("SELECT ID,MSISDN,MESSAGECOUNT FROM sms_txns "
                                        . "WHERE MESSAGEID is null AND SMSINDEX is null $where "
                                        . "ORDER BY ID DESC "
                                        . "LIMIT 0,".$this->config->item('limitcount'))->result();
    }
    
    function save_route($data,$orgid,$messageBalance){
                
                $this->smsgateway->trans_begin();
            foreach($data as $value){

                $this->smsgateway->query("UPDATE sms_txns SET SMSINDEX='".$value['SMSINDEX']."',MESSAGEID='".$value['MESSAGEID']."',ROUTE='".$value['ROUTE']."' WHERE ID='".$value['ID']."'");
            }
                $this->smsgateway->query("UPDATE message_balance SET REMAININGCOUNT='".$messageBalance."',LASTUPDATE='".date('Y-m-d H:i:s)')."' WHERE ORGID='$orgid'");
            
            if($this->smsgateway->trans_status() === FALSE){
                
                $this->smsgateway->trans_rollback();
                return FALSE;
            }else{
                
                $this->smsgateway->trans_commit();
                return TRUE;
            }
            
    }
    
    function save_file_message_data($data){
        
                $this->smsgateway->trans_start();
            foreach($data as $value){

                $this->smsgateway->insert('sms_txns',$value);
            }
                
                $this->smsgateway->trans_complete();
        return $this->smsgateway->trans_status();
    }
}