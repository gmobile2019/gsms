<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['admin_panel']=array(
                            array('link'=>'Admin/','title'=>'Home','cls'=>'hme'),
                            array('link'=>'Admin/organizations','title'=>'Organizations','cls'=>'org'),
                            array('link'=>'Admin/subscriptions','title'=>'Subscriptions','cls'=>'sub'),
                            array('link'=>'Admin/senderids','title'=>'Sender IDs','cls'=>'sID'),
                            array('link'=>'Admin/messages','title'=>'Messages','cls'=>'msg'),
                            array('link'=>'Admin/report','title'=>'Report','cls'=>'rprt'),
                            array('link'=>'Admin/profile','title'=>'Profile','cls'=>'prfl'),
                        );

$config['user_panel']=array(
                            array('link'=>'User/','title'=>'Home','cls'=>'hme'),
                            array('link'=>'User/messages','title'=>'Messages','cls'=>'msg'),
                            array('link'=>'User/subscriptions','title'=>'Subscriptions','cls'=>'sub'),
                            array('link'=>'User/report','title'=>'Report','cls'=>'rprt'),
                            array('link'=>'User/accountDetails','title'=>'Account','cls'=>'prfl'),
                        );

$config['maximum_upload_filesize']='4096';

$config['bulk_sms_upload_path']='./uploads/';

$config['variable_string']='%s';

$config['allowed_file_types']='xls|csv';

$config['maximum_message_length']=160;

$config['source_token_string']='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

$config['salt']='gmobile2016';

$config['offset']=3;

$config['portal_user_grp']=2;

$config['keyWord']='Gmobile';

$config['msgid_id_length']=5;

$config['limitcount']=200;

$config['loopcount']=20;

$config['sms_engine_instances']=3;

$config['insertdatacount']=5;

$config['debug']=FALSE;

$config['route1_acc_blnce_url']='https://api.infobip.com/account/1/balance';

$config['route1_username']='Allsee';

$config['route1_password']='Techas12';

$config['route1_authorization_method']='Basic';

