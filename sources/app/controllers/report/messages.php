<?php defined('BASEPATH') OR exit('No direct script access allowed');

$recipient=$recipient<>null?$recipient:"All";
$status=$status<>null?$status:"All";
$org=$org<>null?$data[0]->NAME:"All";
$start=$start<>null?$start:"No Limit";
$end=$end<>null?$end:"No Limit";

$this->excel->setActiveSheetIndex(0);

$this->excel->getActiveSheet()->setTitle('Messages Report-'.date('YmdHis'));
$this->excel->getActiveSheet()->setCellValue('C1','Messages Report');
$this->excel->getActiveSheet()->setCellValue('A2','Recipient');
$this->excel->getActiveSheet()->setCellValueExplicit('B2',$recipient,PHPExcel_Cell_DataType::TYPE_STRING);
$this->excel->getActiveSheet()->setCellValue('A3','Status');
$this->excel->getActiveSheet()->setCellValue('B3',$status);
$this->excel->getActiveSheet()->setCellValue('A4','Organization');
$this->excel->getActiveSheet()->setCellValue('B4',$org);
$this->excel->getActiveSheet()->setCellValue('A5','Start Date');
$this->excel->getActiveSheet()->setCellValue('B5',$start);
$this->excel->getActiveSheet()->setCellValue('A6','End Date');
$this->excel->getActiveSheet()->setCellValue('B6',$end);

$this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
$this->excel->getActiveSheet()->getStyle('B2')->getFont()->setBold(true);
$this->excel->getActiveSheet()->getStyle('B3')->getFont()->setBold(true);
$this->excel->getActiveSheet()->getStyle('B4')->getFont()->setBold(true);
$this->excel->getActiveSheet()->getStyle('B5')->getFont()->setBold(true);
$this->excel->getActiveSheet()->getStyle('B6')->getFont()->setBold(true);

$this->excel->getActiveSheet()->setCellValue('A8','Organization');
$this->excel->getActiveSheet()->setCellValue('B8','Message ID');
$this->excel->getActiveSheet()->setCellValue('C8','Message Count');
$this->excel->getActiveSheet()->setCellValue('D8','Recipient');
$this->excel->getActiveSheet()->setCellValue('E8','Sender ID');
$this->excel->getActiveSheet()->setCellValue('F8','Schedule Time');
$this->excel->getActiveSheet()->setCellValue('G8','Status');


$this->excel->getActiveSheet()->getStyle('A8:G8')->getFont()->setBold(true);
$this->excel->getActiveSheet()->getStyle('A8:G8')->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_DARKGREEN);

$this->excel->getActiveSheet()->getStyle('A8:G8')->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$this->excel->getActiveSheet()->getStyle('A8:G8'.$i)->getBorders()->getInside()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	
if($data !=null){
   
    $i=9;
    $total_a=0;
    foreach($data as $key=>$value){
        
        $this->excel->getActiveSheet()->setCellValue('A'.$i,$value->NAME);
        $this->excel->getActiveSheet()->setCellValueExplicit('B'.$i,$value->MESSAGEID,PHPExcel_Cell_DataType::TYPE_STRING);
        $this->excel->getActiveSheet()->setCellValueExplicit('C'.$i,$value->MESSAGECOUNT,PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $this->excel->getActiveSheet()->setCellValueExplicit('D'.$i,$value->MSISDN,PHPExcel_Cell_DataType::TYPE_STRING);
        $this->excel->getActiveSheet()->setCellValue('E'.$i,$value->SENDERID);
        $this->excel->getActiveSheet()->setCellValueExplicit('F'.$i,$value->SCHEDULETIME,PHPExcel_Cell_DataType::TYPE_STRING);
        $this->excel->getActiveSheet()->setCellValue('G'.$i,$value->STATUS);
        
        $this->excel->getActiveSheet()->getStyle('A'.$i.':G'.$i)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $this->excel->getActiveSheet()->getStyle('A'.$i.':G'.$i)->getBorders()->getInside()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        
        $i++;
    }
    
    
    $filename="Messages-".date('YmdHis').".xls";
    
    
    header('Content-Type: application/vnd.ms-excel'); //mime type
    header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
    header('Cache-Control: max-age=0');//no cache
    
    ob_end_clean();
   
   $objWriter = PHPExcel_IOFactory::createWriter($this->excel,'Excel5');
   $objWriter->save('php://output');
   exit;
}