<?php defined('BASEPATH') OR exit('No direct script access allowed');

$this->pdf->start_pdf();
$this->pdf->SetSubject('gmobile');
$this->pdf->SetKeywords('gmobile');

//start pdf page
$this->pdf->AddPage();
$this->pdf->SetY(35);
$this->pdf->SetX(2);
$this->pdf->SetFont('', '', 8);
//heading
$html  = '<h3 align="center">Message Report</h3>';


    $html.='<table border="1">
                <tr>
                    <td style="width:200px;text-align:center"><b> &nbsp;S/No</b></td>
                    <td style="width:600px;text-align:center"><b> &nbsp;Organization</b></td>
                    <td style="width:600px;text-align:center"><b> &nbsp;Status</b></td>
                    <td style="width:500px;text-align:center"><b> &nbsp;No Messages</b></td>
                </tr>';
$i = 1;
$total=0;
//echo $html;exit;
    foreach ($data as $key => $value) {
        $total +=$value->smsCount;
        $html .='<tr>
                    <td>&nbsp;&nbsp;' . $i++ .'</td>
                    <td>&nbsp;&nbsp;' .$value->NAME . ' &nbsp; </td>
                    <td>&nbsp;&nbsp;' .$value->STATUS . ' &nbsp; </td>
                    <td>&nbsp;&nbsp;'.$value->smsCount.'</td>
                </tr>';
      }

$html.='<tr>
            <td colspan="3" style="text-align:right;font-weight:bold">Total&nbsp;&nbsp;</td>
            <td>&nbsp;&nbsp;'.$total .'</td>
        </tr></table>';
$this->pdf->writeHTML($html);
ob_end_clean();
$this->pdf->Output('Report-'.date('YmdHis').'.pdf', 'D');
exit;
?>
