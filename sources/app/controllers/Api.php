<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Api
 *
 * @author Manuel
 */
class Api extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->model('Api_model');
    }
    
    function normalReceiver(){
        $destination="./logs/normalReceiver_".date('Y-m-d').'.log'; 
        $request =file_get_contents('php://input');
        
        $logfile=file_exists($destination);

        if($logfile){

            $filesize=filesize($destination);

            if($filesize > 10485760){
                rename($destination,"./logs/normalReceiver_".date('His')."_".date('Y-m-d').'.log');
            }
        }
        
        error_log("req : $request ".date('Y-m-d H:i:s')."\n", 3, $destination);
        
        libxml_use_internal_errors(true);
       
        $xml =simplexml_load_string($request);
      
        if (!$xml) {
            error_log("Xml invalid ".date('Y-m-d H:i:s')."\n", 3, $destination);
            $errors = libxml_get_errors();
            
            foreach ($errors as $error) {
                
                error_log("Error : $error->message ".date('Y-m-d H:i:s')."\n", 3, $destination);
               
            }

            libxml_clear_errors();
        }
        
        //verify access token
        $access=$this->Api_model->check_access_credentials((string)trim($xml->token),(string)trim($xml->username),$this->hash_password((string)trim($xml->password)));
        
        if(!$access){
            //invalid access credentials
            error_log("invalid access credentials ".date('Y-m-d H:i:s')."\n", 3, $destination);
            $details=$this->Api_model->get_response_details(103);
            
            $response="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
            $response.="<response><code>".$details->code."</code><status>".$details->status."</status><description>".$details->description."</description></response>";
            
            error_log("Response : $response ".date('Y-m-d H:i:s')."\n", 3, $destination);
            ob_end_clean();
            
            echo $response;
            exit;
        }
       
        $sender=$this->Api_model->sender((string)trim($xml->data->sendercode));
         
        if($sender == null){
            
            //invalid sender code
            error_log("invalid sender code ".date('Y-m-d H:i:s')."\n", 3, $destination);
            $details=$this->Api_model->get_response_details(104);
            
            $response="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
            $response.="<response><code>".$details->code."</code><status>".$details->status."</status><description>".$details->description."</description></response>";
            
            error_log("Response : $response ".date('Y-m-d H:i:s')."\n", 3, $destination);
            ob_end_clean();
            
            echo $response;
            exit;
        }
        
        $messagecount=ceil(strlen(trim((string)trim($xml->data->sms)))/$this->config->item('maximum_message_length'));
        $messagebalance=$this->Administrator_model->messageBalance($sender->ORGID);
        
        if($messagebalance < $messagecount){
            
            //no sms balance
            error_log("no sms balance ".date('Y-m-d H:i:s')."\n", 3, $destination);
            $details=$this->Api_model->get_response_details(106);
            
            $response="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
            $response.="<response><code>".$details->code."</code><status>".$details->status."</status><description>".$details->description."</description></response>";
            
            error_log("Response : $response ".date('Y-m-d H:i:s')."\n", 3, $destination);
            ob_end_clean();
            
            echo $response;
            exit;
        }
        
        $recipient=(string)trim($xml->data->recipient);
        
        if((substr($recipient,0,1) <> '+' && substr($recipient,0,1) == 0) || strlen($recipient) == 9 ){
            
            $recipient='255'.substr($recipient,-9);
        }else{
            
            $recipient=  ltrim($recipient,'+');
        }
        
        $timestamp=(string)trim($xml->data->timestamp) <> null && (string)trim($xml->data->timestamp) <> ''?(string)trim($xml->data->timestamp):date('Y-m-d H:i:s');
        
        $sms_txn=array(
                        'SMS'=>trim((string)trim($xml->data->sms)),
                        'ORGID'=>$sender->ORGID,
                        'SENDERID'=>$sender->SENDER,
                        'MSISDN'=>$recipient,
                        'MESSAGECOUNT'=>$messagecount,
                        'ACTIONDATE'=>date('Y-m-d H:i:s'),
                        'STATUS'=>'Pending',
                        'SCHEDULETIME'=>$timestamp,
                   );
        
        if((int)trim($xml->data->dlr) == 1 ){
            
            if((string)trim($xml->data->messageid) == null || (string)trim($xml->data->dlrurl == null)){
                error_log("messageid missing ".date('Y-m-d H:i:s')."\n", 3, $destination);
                $details=$this->Api_model->get_response_details(108);
            
                $response="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
                $response.="<response><code>".$details->code."</code><status>".$details->status."</status><description>".$details->description."</description></response>";

                error_log("Response : $response ".date('Y-m-d H:i:s')."\n", 3, $destination);
                ob_end_clean();

                echo $response;
                exit;
            }
            $sms_txn['THIRDY_PARTY_MSGID']=(string)trim($xml->data->messageid);
            $sms_txn['DLR_URL']=(string)urldecode($xml->data->dlrurl);
            $sms_txn['DLR']=(int)trim($xml->data->dlr);
        }
        
        $sve=$this->Administrator_model->save_message_data2($sms_txn);
        
        if(!$sve){
            
            //internal server error
            error_log("Database Error : Unable to save ".date('Y-m-d H:i:s')."\n", 3, $destination);
            $details=$this->Api_model->get_response_details(105);
            
            $response="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
            $response.="<response><code>".$details->code."</code><status>".$details->status."</status><description>".$details->description."</description></response>";

            error_log("Response : $response ".date('Y-m-d H:i:s')."\n", 3, $destination);
            ob_end_clean();

            echo $response;
            exit;
        }
        
        error_log("successfull ".date('Y-m-d H:i:s')."\n", 3, $destination);
        $details=$this->Api_model->get_response_details(100);
            
        $response="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
        $response.="<response><code>".$details->code."</code><status>".$details->status."</status><description>".$details->description."</description></response>";

        error_log("Response : $response ".date('Y-m-d H:i:s')."\n", 3, $destination);
        ob_end_clean();

        echo $response;
        exit;
    }
    
    function hash_password($password){
        
        $strlen=strlen($password);
        $length=$strlen-$this->config->item('offset');
        
        $salt=substr($this->config->item('salt'),$this->config->item('offset'), $length);
        
        return md5($password.$salt);
    }
}