<?php defined('BASEPATH') OR exit('No direct script access allowed');
$this->excel->setActiveSheetIndex(0);

$this->excel->getActiveSheet()->setTitle('PersonalizedMessageTemplate');
$this->excel->getActiveSheet()->setCellValue('A1','Recipient');
$this->excel->getActiveSheet()->setCellValue('B1','Variable1');
$this->excel->getActiveSheet()->setCellValue('C1','Variable2');
$this->excel->getActiveSheet()->setCellValue('D1','Variable3');

$this->excel->getActiveSheet()->getStyle('A1:D1')->getFont()->setBold(true);
$this->excel->getActiveSheet()->getStyle('A1:D1')->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_DARKGREEN);

$this->excel->getActiveSheet()->getStyle('A1:D1')->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$this->excel->getActiveSheet()->getStyle('A1:D1'.$i)->getBorders()->getInside()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$i=2;
$this->excel->getActiveSheet()->setCellValueExplicit('A'.$i,'255xxxxxx',PHPExcel_Cell_DataType::TYPE_STRING);
$this->excel->getActiveSheet()->setCellValueExplicit('B'.$i,'XY',PHPExcel_Cell_DataType::TYPE_STRING);
$this->excel->getActiveSheet()->setCellValueExplicit('C'.$i,'GS',PHPExcel_Cell_DataType::TYPE_STRING);
$this->excel->getActiveSheet()->setCellValueExplicit('D'.$i,'Hk',PHPExcel_Cell_DataType::TYPE_STRING);
$this->excel->getActiveSheet()->getStyle('A'.$i.':D'.$i)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$this->excel->getActiveSheet()->getStyle('A'.$i.':D'.$i++)->getBorders()->getInside()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

$this->excel->getActiveSheet()->setCellValueExplicit('A'.$i,'255xxxxxx',PHPExcel_Cell_DataType::TYPE_STRING);
$this->excel->getActiveSheet()->setCellValueExplicit('B'.$i,'XY',PHPExcel_Cell_DataType::TYPE_STRING);
$this->excel->getActiveSheet()->setCellValueExplicit('C'.$i,'GS',PHPExcel_Cell_DataType::TYPE_STRING);
$this->excel->getActiveSheet()->setCellValueExplicit('D'.$i,'Hk',PHPExcel_Cell_DataType::TYPE_STRING);
$this->excel->getActiveSheet()->getStyle('A'.$i.':D'.$i)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$this->excel->getActiveSheet()->getStyle('A'.$i.':D'.$i++)->getBorders()->getInside()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

$this->excel->getActiveSheet()->setCellValueExplicit('A'.$i,'255xxxxxx',PHPExcel_Cell_DataType::TYPE_STRING);
$this->excel->getActiveSheet()->setCellValueExplicit('B'.$i,'XY',PHPExcel_Cell_DataType::TYPE_STRING);
$this->excel->getActiveSheet()->setCellValueExplicit('C'.$i,'GS',PHPExcel_Cell_DataType::TYPE_STRING);
$this->excel->getActiveSheet()->setCellValueExplicit('D'.$i,'Hk',PHPExcel_Cell_DataType::TYPE_STRING);
$this->excel->getActiveSheet()->getStyle('A'.$i.':D'.$i)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$this->excel->getActiveSheet()->getStyle('A'.$i.':D'.$i++)->getBorders()->getInside()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

$this->excel->getActiveSheet()->setCellValueExplicit('A'.$i,'255xxxxxx',PHPExcel_Cell_DataType::TYPE_STRING);
$this->excel->getActiveSheet()->setCellValueExplicit('B'.$i,'XY',PHPExcel_Cell_DataType::TYPE_STRING);
$this->excel->getActiveSheet()->setCellValueExplicit('C'.$i,'GS',PHPExcel_Cell_DataType::TYPE_STRING);
$this->excel->getActiveSheet()->setCellValueExplicit('D'.$i,'Hk',PHPExcel_Cell_DataType::TYPE_STRING);
$this->excel->getActiveSheet()->getStyle('A'.$i.':D'.$i)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$this->excel->getActiveSheet()->getStyle('A'.$i.':D'.$i++)->getBorders()->getInside()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

$this->excel->getActiveSheet()->setCellValueExplicit('A'.$i,'255xxxxxx',PHPExcel_Cell_DataType::TYPE_STRING);
$this->excel->getActiveSheet()->setCellValueExplicit('B'.$i,'XY',PHPExcel_Cell_DataType::TYPE_STRING);
$this->excel->getActiveSheet()->setCellValueExplicit('C'.$i,'GS',PHPExcel_Cell_DataType::TYPE_STRING);
$this->excel->getActiveSheet()->setCellValueExplicit('D'.$i,'Hk',PHPExcel_Cell_DataType::TYPE_STRING);
$this->excel->getActiveSheet()->getStyle('A'.$i.':D'.$i)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$this->excel->getActiveSheet()->getStyle('A'.$i.':D'.$i++)->getBorders()->getInside()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

$filename="PersonalizedMessageTemplate.xls";
    
    
header('Content-Type: application/vnd.ms-excel'); //mime type
header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
header('Cache-Control: max-age=0');//no cache

ob_end_clean();

$objWriter = PHPExcel_IOFactory::createWriter($this->excel,'Excel5');
$objWriter->save('php://output');
exit;