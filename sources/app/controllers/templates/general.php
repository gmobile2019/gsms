<?php defined('BASEPATH') OR exit('No direct script access allowed');
$this->excel->setActiveSheetIndex(0);

$this->excel->getActiveSheet()->setTitle('GeneralMessageTemplate');
$this->excel->getActiveSheet()->setCellValue('A1','Recipient');

$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
$this->excel->getActiveSheet()->getStyle('A1')->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_DARKGREEN);

$this->excel->getActiveSheet()->getStyle('A1')->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$this->excel->getActiveSheet()->getStyle('A1'.$i)->getBorders()->getInside()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$i=2;
$this->excel->getActiveSheet()->setCellValueExplicit('A'.$i,'255xxxxxx',PHPExcel_Cell_DataType::TYPE_STRING);
$this->excel->getActiveSheet()->getStyle('A'.$i)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$this->excel->getActiveSheet()->getStyle('A'.$i++)->getBorders()->getInside()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

$this->excel->getActiveSheet()->setCellValueExplicit('A'.$i,'255xxxxxx',PHPExcel_Cell_DataType::TYPE_STRING);
$this->excel->getActiveSheet()->getStyle('A'.$i)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$this->excel->getActiveSheet()->getStyle('A'.$i++)->getBorders()->getInside()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

$this->excel->getActiveSheet()->setCellValueExplicit('A'.$i,'255xxxxxx',PHPExcel_Cell_DataType::TYPE_STRING);
$this->excel->getActiveSheet()->getStyle('A'.$i)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$this->excel->getActiveSheet()->getStyle('A'.$i++)->getBorders()->getInside()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

$this->excel->getActiveSheet()->setCellValueExplicit('A'.$i,'255xxxxxx',PHPExcel_Cell_DataType::TYPE_STRING);
$this->excel->getActiveSheet()->getStyle('A'.$i)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$this->excel->getActiveSheet()->getStyle('A'.$i++)->getBorders()->getInside()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

$this->excel->getActiveSheet()->setCellValueExplicit('A'.$i,'255xxxxxx',PHPExcel_Cell_DataType::TYPE_STRING);
$this->excel->getActiveSheet()->getStyle('A'.$i)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$this->excel->getActiveSheet()->getStyle('A'.$i++)->getBorders()->getInside()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

$filename="GeneralMessageTemplate.xls";
    
    
header('Content-Type: application/vnd.ms-excel'); //mime type
header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
header('Cache-Control: max-age=0');//no cache

ob_end_clean();

$objWriter = PHPExcel_IOFactory::createWriter($this->excel,'Excel5');
$objWriter->save('php://output');
exit;