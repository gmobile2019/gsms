<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Description of Admin
 *
 * @author Manuel
 */
class Admin extends CI_Controller{
    
    function __construct(){
        
            parent::__construct();
            $this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
            $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
            $this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
            $this->output->set_header('Pragma: no-cache');
    }
    
    function index(){
        if (!$this->check_session_account_validity()){

                   //redirect them to the login page
                   redirect('Auth/logout', 'refresh');
           }
            
        $this->data['panel']=$this->config->item('admin_panel');
        $this->data['cls']='hme';
        $this->data['title']='Home';
        $this->data['gatewayBalance']=$this->getSMSBalance(TRUE);
        $this->data['organizations']=$this->Administrator_model->companies();
        $this->data['content']='admin/dashboard';
        $this->load->view('admin/template',$this->data);
    }
    
    function organizations(){
        if (!$this->check_session_account_validity()){

                   //redirect them to the login page
                   redirect('Auth/logout', 'refresh');
           }
            
        $this->data['panel']=$this->config->item('admin_panel');
        
        if ($this->input->post('name')) {
            $key['name'] = $this->input->post('name');
            $this->data['name']=$this->input->post('name');
        }

        if ($this->input->post('status')) {
            $key['status'] = $this->input->post('status');
            $this->data['status']=$this->input->post('status');
        }

       if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['name'] = $exp[1];
            $this->data['name']=$key['name'];

            $key['status'] = $exp[3];
            $this->data['status']=$key['status'];
        }

        $name =$key['name'];
        $status =$key['status'];

        $config["base_url"] = base_url() . "index.php/Admin/organizations/name_".$key['name']."_status_".$key['status']."/";
        $config["total_rows"] =$this->Administrator_model->registered_companies_count($name,$status);
        $config["per_page"] =20;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '<span class="btn btn-info">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['data'] = $this->Administrator_model->registered_companies($name,$status,$page,$limit);
                
        $this->data['cls']='org';
        $this->data['gatewayBalance']=$this->getSMSBalance(TRUE);
        $this->data['title']='Organizations';
        $this->data['content']='admin/orgs';
        $this->load->view('admin/template',$this->data);
    }
    
    function register_org($id){
        if (!$this->check_session_account_validity()){

                   //redirect them to the login page
                   redirect('Auth/logout', 'refresh');
           }
        
        $this->data['panel']=$this->config->item('admin_panel');
        $this->form_validation->set_rules('institutionname','Institution Name','required|xss_clean');
        $this->form_validation->set_rules('institutionaddress','Institution Address','xss_clean|required');
        $this->form_validation->set_rules('institutioncontact','Institution Contact','required|xss_clean');
        $this->form_validation->set_rules('email','Contact Email','required|xss_clean|valid_email');

         if ($this->form_validation->run() == TRUE){

             $institutionname=$this->input->post('institutionname');
             $institutionaddress=$this->input->post('institutionaddress');
             $institutioncontact=$this->input->post('institutioncontact');
             $email=$this->input->post('email');


             $data=array(
                 'NAME'=>$institutionname,
                 'ADDRESS'=>$institutionaddress,
                 'PHONE'=>$institutioncontact,
                 'EMAIL'=>$email,
             );
            
              if($this->Administrator_model->register_company($data,NULL,$id)){

                  $this->data['message']='<div class="alert alert-success " role="alert" style="text-align:center;">data saved!</div>';

              }else{

                  $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">saving failed!</div>';
              }
        }
        
        $this->data['gatewayBalance']=$this->getSMSBalance(TRUE);
        $this->data['data']=$this->Administrator_model->companies($id);
        $this->data['id']=$id;
        $this->data['cls']='org';
        $this->data['title']='Organization Registration';
        $this->data['content']='admin/register_org';
        $this->load->view('admin/template',$this->data);
    }
    
    function activate_deactivate_org($id,$status){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('Auth/logout', 'refresh');
            }

            $this->Administrator_model->activate_deactivate_companies($id,$status);
            redirect('Admin/organizations','refresh');
    }
    
    function register_subscription(){
        $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
        if (!$this->check_session_account_validity())
        {
                //redirect them to the login page
                redirect('Auth/logout', 'refresh');
        }

        $this->data['panel']=$this->config->item('admin_panel');

        $this->form_validation->set_rules('org','Organization','required|xss_clean');
        $this->form_validation->set_rules('messagecount','messagecount','required|xss_clean');

        if ($this->form_validation->run() == TRUE){

            $org=$this->input->post('org');
            $messagecount=$this->input->post('messagecount');
            
            $subscription=array(
                'ORGID'=>$org,
                'MESSAGECOUNT'=>$messagecount,
                'LASTUPDATE'=>date('Y-m-d H:i:s'),
                'USERID'=>$this->session->userdata('user_id')
            );

           if($this->Administrator_model->save_subscription($subscription)){

                  $this->data['message']='<div class="alert alert-success " role="alert" style="text-align:center;">data saved!</div>';

            }else{

                $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">saving failed!</div>';
            }
        }

        $this->data['gatewayBalance']=$this->getSMSBalance(TRUE);
        $this->data['cls']="sub";
        $this->data['title']="Account Top Up";
        $this->data['organizations']=$this->Administrator_model->companies(null,'Active');
        $this->data['content']='admin/register_subscription';
        $this->load->view('admin/template',$this->data);
    }
        
    function subscriptions(){
        if (!$this->check_session_account_validity()){
                //redirect them to the login page
                redirect('Auth/logout', 'refresh');
        }

        $this->data['panel']=$this->config->item('admin_panel');

        if ($this->input->post('org')) {
            $key['org'] = $this->input->post('org');
            $this->data['org']=$this->input->post('org');
        }

      
        if ($this->input->post('start')) {
            $key['start'] = $this->input->post('start');
            $this->data['start']=$this->input->post('start');
        }
        
        if ($this->input->post('end')) {
            $key['end'] = $this->input->post('end');
            $this->data['end']=$this->input->post('end');
        }

       if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['org'] = $exp[1];
            $this->data['org']=$key['org'];

            $key['start'] = $exp[3];
            $this->data['start']=$key['start'];

            $key['end'] = $exp[5];
            $this->data['end']=$key['end'];
        }

        $org =$key['org'];
        $start =$key['start'];
        $end =$key['end'];

        $config["base_url"] = base_url() . "index.php/Admin/subscriptions/org_".$key['org']."_start_".$key['start']."_end_".$key['end']."/";
        $config["total_rows"] =$this->Administrator_model->subscriptions_count($org,$start,$end);
        $config["per_page"] =20;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '<span class="btn btn-info">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['data'] = $this->Administrator_model->subscriptions_info($org,$start,$end,$page,$limit);
        
        $this->data['gatewayBalance']=$this->getSMSBalance(TRUE);
        $this->data['cls']="sub";
        $this->data['title']="Subscriptions";
        $this->data['organizations']=$this->Administrator_model->companies();
        $this->data['content']='admin/subscriptions';
        $this->load->view('admin/template',$this->data);
    }
        
    function senderids(){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('Auth/logout', 'refresh');
            }

        $this->data['panel']=$this->config->item('admin_panel');

        if ($this->input->post('org')) {
            $key['org'] =$this->input->post('org');
            $this->data['org']=$this->input->post('org');
        }

        if ($this->input->post('sender')) {
            $key['sender'] = $this->input->post('sender');
            $this->data['sender']=$this->input->post('sender');
        }



       if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['org'] =$this->input->post('org');
            $this->data['org']=$key['org'];

            $key['sender'] = $exp[3];
            $this->data['sender']=$key['sender'];
        }

        $org =$key['org'];
        $sender =$key['sender'];

        $config["base_url"] = base_url() . "index.php/Admin/senderids/org_".$key['Org']."_sender_".$key['sender']."/";
        $config["total_rows"] =$this->Administrator_model->sender_ids_count($org,$sender);
        $config["per_page"] =20;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '<span class="btn btn-info">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['data'] = $this->Administrator_model->sender_ids($org,$sender,$page,$limit);
        
        $this->data['gatewayBalance']=$this->getSMSBalance(TRUE);
        $this->data['title']="Sender IDs";
        $this->data['cls']="sID";
        $this->data['organizations']=$this->Administrator_model->companies();
        $this->data['content']='admin/senderids';
        $this->load->view('admin/template',$this->data);
   }
       
    function register_senderid($id){
        $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity()){
                     //redirect them to the login page
                     redirect('Auth/logout', 'refresh');
             }

        $this->data['panel']=$this->config->item('admin_panel');

        $this->form_validation->set_rules('org','Organization','required|xss_clean');
        $this->form_validation->set_rules('sender','Sender Id','required|xss_clean|max_length[11]');

        if($this->form_validation->run() == true){

            $array=array(
                'ORGID'=>$this->input->post('org'),
                'SENDER'=>$this->input->post('sender'),
            );

            if($this->Administrator_model->create_sender_id($array,$id)){

                  $this->data['message']='<div class="alert alert-success " role="alert" style="text-align:center;">data saved!</div>';

            }else{

                $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">saving failed!</div>';
            }
        }
        
        $this->data['gatewayBalance']=$this->getSMSBalance(TRUE);
        $this->data['title']="Sender ID Registration";
        $this->data['id']=$id;
        $this->data['cls']="sID";
        $this->data['senders'] =$this->Administrator_model->senders(null,$id);
        $this->data['organizations']=$this->Administrator_model->companies(NULL,'Active');
        $this->data['content']='admin/register_senderid';
        $this->load->view('admin/template',$this->data);
    }
    
    function activate_deactivate_senderid($id,$status){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('Auth/logout', 'refresh');
            }

            $this->Administrator_model->activate_deactivate_senderid($id,$status);
            redirect('Admin/senderids','refresh');
    }
    
    function messages(){
        if (!$this->check_session_account_validity()){

                   //redirect them to the login page
                   redirect('Auth/logout', 'refresh');
           }
            
        $this->data['panel']=$this->config->item('admin_panel');
        
        if ($this->input->post('org')) {
            $key['org'] = $this->input->post('org');
            $this->data['org']=$this->input->post('org');
        }
      
        if ($this->input->post('start')) {
            $key['start'] = $this->input->post('start');
            $this->data['start']=$this->input->post('start');
        }
        
        if ($this->input->post('end')) {
            $key['end'] = $this->input->post('end');
            $this->data['end']=$this->input->post('end');
        }
        
        if ($this->input->post('status')) {
            $key['status'] = $this->input->post('status');
            $this->data['status']=$this->input->post('status');
        }
        
        if ($this->input->post('recipient')) {
            $key['recipient'] = $this->input->post('recipient');
            $this->data['recipient']=$this->input->post('recipient');
        }

       if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['org'] = $exp[1];
            $this->data['org']=$key['org'];

            $key['start'] = $exp[3];
            $this->data['start']=$key['start'];

            $key['end'] = $exp[5];
            $this->data['end']=$key['end'];
            
            $key['status'] = $exp[7];
            $this->data['status']=$key['status'];
            
            $key['recipient'] = $exp[9];
            $this->data['recipient']=$key['recipient'];
            
            $doc = $exp[11];
        }

        $org =$key['org'];
        $start =$key['start'];
        $end =$key['end'];
        $status =$key['status'];
        $recipient =$key['recipient'];

        if($doc == 2){
            
            $data=$this->Administrator_model->messages($org,$start,$end,$status,$recipient);
            require_once 'report/messages.php';
        }
        
        $config["base_url"] = base_url() . "index.php/Admin/messages/org_".$key['org']."_start_".$key['start']."_end_".$key['end']."_status_".$key['status']."_recipient_".$key['recipient']."/";
        $config["total_rows"] =$this->Administrator_model->messages_count($org,$start,$end,$status,$recipient);
        $config["per_page"] =20;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '<span class="btn btn-info">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['data'] = $this->Administrator_model->messages_info($org,$start,$end,$status,$recipient,$page,$limit);
        
        $this->data['gatewayBalance']=$this->getSMSBalance(TRUE);
        $this->data['organizations']=$this->Administrator_model->companies();
        $this->data['msg_status']=$this->Administrator_model->messageStatus();
        $this->data['cls']='msg';
        $this->data['title']='Messages';
        $this->data['content']='admin/messages';
        $this->load->view('admin/template',$this->data);
    }
    
    function report(){
        $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity()){
                     //redirect them to the login page
                     redirect('Auth/logout', 'refresh');
             }

        $this->data['panel']=$this->config->item('admin_panel');

        $this->form_validation->set_rules('startdate','Start Date','required|xss_clean');
        $this->form_validation->set_rules('enddate','End Date','required|xss_clean');
        $this->form_validation->set_rules('org','Organization','xss_clean');
        $this->form_validation->set_rules('status','Status','xss_clean');

        if($this->form_validation->run() == true){
            
            $startdate=$this->input->post('startdate');
            $enddate=$this->input->post('enddate');
            $org=$this->input->post('org');
            $status=$this->input->post('status');
            
            $data=$this->Administrator_model->report($startdate,$enddate,$org,$status);
            if($data <> NULL){
                   
                  require_once 'report/report.php';

            }else{

                $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">No data Found!</div>';
            }
        }
        
        $this->data['gatewayBalance']=$this->getSMSBalance(TRUE);
        $this->data['cls']="rprt";
        $this->data['title']="Report";
        $this->data['msg_status']=$this->Administrator_model->messageStatus();
        $this->data['organizations']=$this->Administrator_model->companies();
        $this->data['content']='admin/report';
        $this->load->view('admin/template',$this->data);
    }
    
    function accountDetails($org){
        if (!$this->check_session_account_validity()){

                   //redirect them to the login page
                   redirect('Auth/logout', 'refresh');
           }
        
        $this->data['gatewayBalance']=$this->getSMSBalance(TRUE);
        $this->data['panel']=$this->config->item('admin_panel');
        $this->data['data']=$this->Administrator_model->account_details($org);
        $this->data['title']='Account Details';
        $this->data['cls']="org";
        $this->data['content']='admin/accountDetails';
        $this->load->view('admin/template',$this->data);
    }
    
    function profile(){
        if (!$this->check_session_account_validity()){
                //redirect them to the login page
                redirect('Home/logout', 'refresh');
        }
        
        $this->data['panel']=$this->config->item('admin_panel');
        $this->data['gatewayBalance']=$this->getSMSBalance(TRUE);
        $this->data['cls']="prfl";
        $this->data['title']="User Profile";
        $this->data['data']=$this->Administrator_model->profile_data();
        $this->data['content']='admin/profile';
        $this->load->view('admin/template',$this->data);
    }
    
    function save_profile(){
       
        $dbField=$this->input->post('name');
        $dbValue=trim($this->input->post('value'));
       
        if($dbValue == NULL || $dbValue == ''){
            
            echo '<span class="alert alert-danger" role="alert" style="text-align:center;">no data to be saved!</span>';
            exit;
        }
       
        if($dbField == 'fname'){
            
            $dbField='FIRST_NAME';
        }
        
        if($dbField == 'mname'){
            
            $dbField='MIDDLE_NAME';
        }
        
        if($dbField == 'surname'){
            
            $dbField='LAST_NAME';
        }
        
        if($dbField == 'mobile'){
            
            $dbField='MSISDN';
        }
        
        if($dbField == 'username'){
            
            $dbField='USERNAME';
        }
        
        $account=array(
            $dbField=>$dbValue,
            'MODIFIEDBY'=>$this->session->userdata('user_id'),
            'MODIFIEDON'=>date('Y-m-d H:i:s')
        );
       
        $sve=$this->Administrator_model->save_accountDetails($account,$this->session->userdata('user_id'));
        
        if(!$sve){
           
            echo '<span class="alert alert-danger" role="alert" style="text-align:center;">error occured!</span>';
            exit;
        }
        
        echo 'saved';
        exit;
    }
    
    function getPieData(){
        
        $pieData=$this->Administrator_model->getPieData($this->input->post('org'));
        $str="";
        foreach($pieData as $key=>$value){
            
            $str .=$value->STATUS.'::'.$value->smsCount.',';
        }
        
        echo rtrim($str, ',');
        exit;
    }
    
    function getBarData(){
       
        $data=$this->Administrator_model->getBarData($this->input->post('org'));
        $str="";
        foreach($data as $key=>$value){
            
            $str .=$value->NAME.'::'.$value->REMAININGCOUNT.',';
        }
        
        echo rtrim($str, ',');
        exit;
    }
    
    function getPendingFileData(){
       
        $data=$this->Administrator_model->getPendingFileData($this->input->post('org'));
    
        $str="<table class='table table-bordered table-hover'>"
                . "<tr>"
                . "<th>No</th>"
                . "<th>Organization</th>"
                . "<th>System File Name</th>"
                . "<th>Sender ID</th>"
                . "<th>Schedule Time</th>"
                . "</tr>";
        
        if($data <> NULL){
            $i=1;
            foreach($data as $key=>$value){
                
                $str .="<tr>"
                        . "<td>&nbsp;&nbsp;".$i++."</td>"
                        . "<td>&nbsp;&nbsp;$value->NAME</td>"
                        . "<td>&nbsp;&nbsp;$value->filename</td>"
                        . "<td>&nbsp;&nbsp;$value->senderid</td>"
                        . "<td>&nbsp;&nbsp;$value->scheduletime</td>"
                        . "</tr>";
            
            } 
        }else{
           $str .="<tr><td colspan='5' style='text-align:center'>No Pending Files</td></tr>"; 
        }
        
        
        $str .="</table>";
        echo trim($str);
        exit;
    }
    
    function getSMSBalance($fetch){
       
        if($fetch){
            
            $fp=fopen('./smsBalance.txt', 'r');
            
            if($fp){
                
                $balance=fgets($fp);
            }
            
            fclose($fp);
            return $balance;
        }
        
        $balance=$this->route1_account_balance();
        
        $fp=  fopen('./smsBalance.txt', 'w');
        fwrite($fp, $balance);
        fclose($fp);
        
        echo $balance;
        exit;
    }
    
    function route1_account_balance(){
        $destination="./logs/acc_balance_".date('Y-m-d').'.log';
         
        $encrypt=$this->encrypt_credentials();
        $authorization=$this->config->item('route1_authorization_method').' '.$encrypt;

        $header= array('Content-Type: application/json','Authorization: '.$authorization);
        $body="";
        $ch=curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->config->item('route1_acc_blnce_url'));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch,CURLOPT_TIMEOUT,5);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response=curl_exec($ch);
        $c_errno=curl_errno($ch);
        $c_err=curl_error($ch);
        
        error_log("connection details : ".$c_errno."(".$c_err.") ".date('Y-m-d H:i:s')."\n", 3, $destination);
        
        curl_close($ch);

        error_log("response : ".$response." ".date('Y-m-d H:i:s')."\n", 3, $destination);
        
        if($c_errno == 0){
            $data=json_decode($response);
            return (string)number_format($data->balance).' '.(string)$data->currency;
        }
        
        return FALSE;
    }
    
    function check_session_account_validity(){
        if (!$this->ion_auth->logged_in()){
                return FALSE;
        }

        if(!$this->ion_auth->in_group('Administrator')){

            return FALSE;
        }

        return TRUE;
   }
   
   function encrypt_credentials(){
        
        $sec_string=$this->config->item('route1_username').':'.$this->config->item('route1_password');
        $encoded_sec_string=base64_encode($sec_string);
        
        return $encoded_sec_string;
    }
    
}