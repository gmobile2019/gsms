<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

    
    function index()
    {

        if (!$this->ion_auth->logged_in())
        {
                //redirect them to the login page
                redirect('Auth/login', 'refresh');
        }else{
            
                redirect('Auth/logout', 'refresh');
        }
    }
    
    //log the user in
    function login(){
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->data['title'] = "Login";

        //validate form input
        $this->form_validation->set_rules('usrnme', 'Username', 'required');
        $this->form_validation->set_rules('pswd', 'Password', 'required');

        if ($this->form_validation->run() == true){ //check to see if the user is logging in
            
            $remember = (bool) $this->input->post('remember');

            if ($this->ion_auth->login($this->input->post('usrnme'), $this->input->post('pswd'), $remember))
            { //if the login is successful
                    //redirect them to a specific  module

                    $this->session->set_flashdata('message', $this->ion_auth->messages());

                    if($this->ion_auth->in_group('Administrator')){

                        redirect('Admin/', 'refresh');
                    }

                    if($this->ion_auth->in_group('User')){

                        redirect('User/', 'refresh');  
                    }
            }else{ 

                    $this->data['message']=$this->ion_auth->errors();
            }
        }
            
        $this->load->view('home', $this->data);
    }

    //log the user out
    function logout(){

        //log the user out
        $this->ion_auth->logout();

        //redirect them back to the page they came from
        redirect('Auth/login','refresh');
    }

}
