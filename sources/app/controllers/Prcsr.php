<?php

class Prcsr extends CI_Controller{
    
    function __construct(){
        
            parent::__construct();
            $this->load->model('Prcsr_model');
            
    }
    
    function file_processor(){
        $destination="./logs/file_processor_".date('Y-m-d').'.log';
        
        $lock =file_exists('./locks/fprocessor.lock');
        if ($lock) {

            echo "LOCKED\n";
            exit;

        }else{
            
            $lk=fopen('./locks/fprocessor.lock','w'); 
        }
        
        $files=$this->Prcsr_model->pending_files();
        
		if($files <> NULL){
			echo "file count : ".count($files)." ".date('Y-m-d H:i:s')."\n";
			error_log("file count : ".count($files)." ".date('Y-m-d H:i:s')."\n", 3, $destination);
			
			foreach($files as $key=>$value){
				
				$filename=$value->filename.".".$value->fext;
				
				if($value->fext <> "csv"){
					
					$reader= PHPExcel_IOFactory::createReader('Excel5');
					if($value->fext == 'xlsx'){
						
						$reader= PHPExcel_IOFactory::createReader('Excel2007');
					}
					 
					$excel=$reader->load($this->config->item('bulk_sms_upload_path').$value->filename.".".$value->fext);
					$objWriter = PHPExcel_IOFactory::createWriter($excel, 'CSV');
					$objWriter->save($this->config->item('bulk_sms_upload_path').$value->filename.'.csv');
					
					
					$filename=$value->filename.'.csv';
				}
				
			   
				$file = file($this->config->item('bulk_sms_upload_path').$filename, FILE_SKIP_EMPTY_LINES);
				if ($file) {
					
					echo "$filename|$value->senderid|$value->smstype|$value->smsbatch| ".date('Y-m-d H:i:s')."\n";
					error_log("$filename|$value->senderid|$value->smstype|$value->smsbatch| ".date('Y-m-d H:i:s')."\n", 3, $destination);
					$sms_txn=array();
					foreach ($file as $row=>$line) {
						
						if($line == " " || $line == NULL){
							
							echo "empty line \n";
							error_log("empty line", 3, $destination);
							continue;
						}
						
						$recipient = str_replace(['+', '-'],'',filter_var($line, FILTER_SANITIZE_NUMBER_INT));
						$message=$value->message;
						
						$info=null;
						
						if($value->smstype == 'Personalized'){
							
								$line=str_replace('"','',$line);
								$data=explode(",", $line);
								$msg_var_count=count($data)-1;

								$variables=array();
								for($v=1;$v<=$msg_var_count;$v++){
													
									$variables[]=trim($data[$v],'"');
								}

							   $info=$this->compose_sms_variables($value->message,$variables,$msg_var_count);//composes the sms returning respective variables
							   
							   $message=trim($info['Message']);
							   $recipient = str_replace(['+', '-'],'',filter_var($data[0],FILTER_SANITIZE_NUMBER_INT));
						}
						
						$message=trim(preg_replace('/\s+/',' ', $message));
						
						if($recipient == " " || $recipient == NULL){
							
							echo "invalid mobile # \n";
							error_log("invalid mobile #", 3, $destination);
							continue;
						}
					   
						if(substr($recipient, 0,1) == 0){
								
								$recipient='255'.substr($recipient,-9);
						}else if(strlen($recipient) == 9){

								$recipient='255'.substr($recipient,-9);
						}else{
							$recipient=$recipient;
						}
						
						
						$sms_txn[]=array(
									'SMS'=>$message,
									'ORGID'=>$value->orgid,
									'USERID'=>$value->createdby,
									'SENDERID'=>$value->senderid,
									'MSISDN'=>trim($recipient,"+"),
									'MESSAGECOUNT'=>ceil(strlen(trim($message))/$this->config->item('maximum_message_length')),
									'ACTIONDATE'=>date('Y-m-d H:i:s'),
									'SMSBATCH'=>$value->smsbatch,
									'STATUS'=>'Pending',
									'SCHEDULETIME'=>$value->scheduletime,
								);
						
					}
					
					while(TRUE){
						$sve=$this->Prcsr_model->save_file_message_data($sms_txn);

						if($sve){
							break;
						}
					}
					
					$this->Administrator_model->update_sms_batch($value->companyid);
					$this->Prcsr_model->update_file_status($value->id);
					echo "file reading complete ".date('Y-m-d H:i:s')."\n";
					error_log("file reading complete ".date('Y-m-d H:i:s')."\n", 3, $destination);
					 
				} else {
					echo "can't open file ".date('Y-m-d H:i:s')."\n";
					error_log("can't open file ".date('Y-m-d H:i:s')."\n", 3, $destination);
				} 
			}
        }
        fclose($lk);
        unlink('./locks/fprocessor.lock');
    }
    
    function compose_sms_variables($message,$variables,$count){
             
        $array=array();
        $offset=0;
        $newMessage='';
        $j=1;
        $strlen=strlen($message);

        for($i=0;$i < $count;$i++){



            $array["variable".$j]=$variables[$i];

            $pos=strpos($message,$this->config->item('variable_string'),$offset);//getting the position of variable string

            if($i==0 && ($i+1 <> $count)){
               $substrln=$pos+2;
            }else if($i+1 == $count){

               $substrln=$strlen-$offset;
            }else{

                $substrln=$pos-$offset+2;
            }


            $substring=substr($message,$offset,$substrln);//getting a string block with single occurance of variable string

            $substring=str_replace($this->config->item('variable_string'),$variables[$i],$substring);//replacing the occurance with a variable

            $newMessage .=$substring; //composing a new message
            $offset=$pos+2;
            $j++;
        }

        $array['Message']=trim($newMessage);
        return $array;
   }
       
    function trafficRouter(){
       $destination="./logs/t_router_".date('Y-m-d').'.log';
        
        $lock =file_exists('./locks/t_router.lock');
        if ($lock) {

            echo "LOCKED\n";
            exit;

        }else{
            
            $lk=fopen('./locks/t_router.lock','w'); 
        }
        $i=1;
        while($i++ <= $this->config->item('loopcount')){
            
            $orgs=$this->Administrator_model->companies(NULL,'Active');
            
            foreach($orgs AS $ky=>$val){
                $messageBalance=$this->Administrator_model->messageBalance($val->ID)->REMAININGCOUNT;
                
                $data=$this->Prcsr_model->getSmsPendings($val->ID);
            
                if($data <> NULL){
                    $j=1;
                    $arr=array();
                    foreach($data as $key=>$value){
                        
                        if($messageBalance < $value->MESSAGECOUNT){
                            
                            echo "$val->NAME low balance [$messageBalance] \n";
                            error_log("$val->NAME low balance [$messageBalance] \n", 3, $destination);
                            continue;
                        }
                        
                        $messageBalance -=$value->MESSAGECOUNT;
                        $messageID=$this->getMessageID();

                        echo "$value->ID|$value->MSISDN|$messageID \n";
                        error_log("$value->ID|$value->MSISDN|$messageID \n", 3, $destination);

                        $index=$j++%$this->config->item('sms_engine_instances');

                        if($index == 0){
                            $index=$this->config->item('sms_engine_instances');
                             $j=1;
                        }

                        $route=1;

                        if(substr(ltrim(trim($value->MSISDN),'+'),0,1) <> 0 && substr(ltrim(trim($value->MSISDN),'+'),0,3) <> 255){

                            $route=2;
                        }

                        $n_strng=substr($value->MSISDN, -9);
                        $n_strng=substr($n_strng,0,3);

                        if(in_array($n_strng, $this->config->item('alternative_route'))){
                            $route=2;
                        }

                        $arr[]=array(
                            'SMSINDEX'=>$index,
                            'MESSAGEID'=>$messageID,
                            'ROUTE'=>$route,
                            'ID'=>$value->ID,
                        );

                    }

                    if($arr <> NULL){
                       
                        $save=$this->Prcsr_model->save_route($arr,$val->ID,$messageBalance);
                        if($save){
                            
                            echo "saved [$messageBalance]".date('Y-m-d H:i:s')."\n";
                            error_log("saved [$messageBalance]".date('Y-m-d H:i:s')."\n", 3, $destination);
                            continue;
                        }
                        echo "not saved ".date('Y-m-d H:i:s')."\n";
                        error_log("not saved ".date('Y-m-d H:i:s')."\n", 3, $destination); 
                    }
                }else{

                    if($this->config->item('debug')){
                        echo "no pendings ".date('Y-m-d H:i:s')."\n";
                        error_log("no pendings ".date('Y-m-d H:i:s')."\n", 3, $destination);
                    }
                }
            }
            
        }
        
        fclose($lk);
        unlink('./locks/t_router.lock');
    }
    
    function getMessageID(){
        $id="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        
        return substr(str_shuffle($id),-$this->config->item('msgid_id_length')).str_replace(" ",".", microtime());
    }
}
