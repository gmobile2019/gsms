<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of User
 *
 * @author Manuel
 */
class User extends CI_Controller {
    //put your code here
    
    function __construct(){
        
            parent::__construct();
            $this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
            $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
            $this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
            $this->output->set_header('Pragma: no-cache');
    }
    
    function index(){
        if (!$this->check_session_account_validity()){

                   //redirect them to the login page
                   redirect('Auth/logout', 'refresh');
           }
            
        $this->data['panel']=$this->config->item('user_panel');
        $this->data['cls']='hme';
        $this->data['title']='Home';
        $this->data['organizations']=$this->Administrator_model->companies();
        $this->data['content']='user/dashboard';
        $this->load->view('user/template',$this->data);
    }
    
    function messages(){
        if (!$this->check_session_account_validity()){

                   //redirect them to the login page
                   redirect('Auth/logout', 'refresh');
           }
            
        $this->data['panel']=$this->config->item('user_panel');
        
        if ($this->input->post('org')) {
            $key['org'] = $this->session->userdata('org');
            $this->data['org']=$this->input->post('org');
        }
      
        if ($this->input->post('start')) {
            $key['start'] = $this->input->post('start');
            $this->data['start']=$this->input->post('start');
        }
        
        if ($this->input->post('end')) {
            $key['end'] = $this->input->post('end');
            $this->data['end']=$this->input->post('end');
        }
        
        if ($this->input->post('status')) {
            $key['status'] = $this->input->post('status');
            $this->data['status']=$this->input->post('status');
        }
        
        if ($this->input->post('recipient')) {
            $key['recipient'] = $this->input->post('recipient');
            $this->data['recipient']=$this->input->post('recipient');
        }

       if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['org'] = $this->session->userdata('org');
            $this->data['org']=$key['org'];

            $key['start'] = $exp[3];
            $this->data['start']=$key['start'];

            $key['end'] = $exp[5];
            $this->data['end']=$key['end'];
            
            $key['status'] = $exp[7];
            $this->data['status']=$key['status'];
            
            $key['recipient'] = $exp[9];
            $this->data['recipient']=$key['recipient'];
            
            $doc = $exp[11];
        }

        $org =$this->session->userdata('org');
        $start =$key['start'];
        $end =$key['end'];
        $status =$key['status'];
        $recipient =$key['recipient'];

        if($doc == 2){
            
            $data=$this->Administrator_model->messages($org,$start,$end,$status,$recipient);
            require_once 'report/messages.php';
        }
        
        $config["base_url"] = base_url() . "index.php/User/messages/org_".$key['org']."_start_".$key['start']."_end_".$key['end']."_status_".$key['status']."_recipient_".$key['recipient']."/";
        $config["total_rows"] =$this->Administrator_model->messages_count($org,$start,$end,$status,$recipient);
        $config["per_page"] =20;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '<span class="btn btn-info">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['data'] = $this->Administrator_model->messages_info($org,$start,$end,$status,$recipient,$page,$limit);
        
        $this->data['organizations']=$this->Administrator_model->companies();
        $this->data['msg_status']=$this->Administrator_model->messageStatus();
        $this->data['cls']='msg';
        $this->data['title']='Messages';
        $this->data['content']='user/messages';
        $this->load->view('user/template',$this->data);
    }
    
    function composeNewMessage($fileType){
        $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
        if (!$this->check_session_account_validity())
        {
                //redirect them to the login page
                redirect('Auth/logout', 'refresh');
        }

        $this->data['panel']=$this->config->item('user_panel');
        if($fileType === "general"){
            
            require_once 'templates/general.php';
        }
        
        if($fileType === "personalized"){
            
            require_once 'templates/personalized.php';
        }
        
        $this->form_validation->set_rules('message','Message','trim|required|xss_clean');
        $this->form_validation->set_rules('bulk','Are you doing a bulk upload?','required|xss_clean');
        $this->form_validation->set_rules('msgcategory','Message Category','required|xss_clean');
        $this->form_validation->set_rules('sender','Message Sender','required|xss_clean');
        $this->input->post('bulk') == 'No'? $this->form_validation->set_rules('recipientsNo','How many recipients?','required|xss_clean'):'';
        $this->form_validation->set_rules('now','Send Instantly','required|xss_clean');
        $this->input->post('now') == 'No'? $this->form_validation->set_rules('scheduleNo','How many schedules?','required|xss_clean'):'';

        if ($this->form_validation->run() == TRUE)
        {
            
            $count_validity=TRUE;
            $sms_txns=array();

            $sms_batch=$this->Administrator_model->sms_batch($this->session->userdata('org'));

            if($this->input->post('bulk') === 'Yes'){

                if($_FILES['bulk_recipients']['name'] <> null){

                    $config['upload_path'] = $this->config->item('bulk_sms_upload_path');
                    $config['allowed_types'] = $this->config->item('allowed_file_types');
                    $config['max_size'] = $this->config->item('maximum_upload_filesize');
                    $filename=explode('.',$_FILES['bulk_recipients']['name']);
                    $excel_ext=end($filename);
                    $excelname=$this->session->userdata('org').date('YmdHis');
                    $orgname='M'.$excelname;
                    $filename='M'.$excelname.".".$excel_ext;
                    $config['file_name']=$filename;
                    $config['overwrite']=true;
                    $this->upload->initialize($config);

                    if($this->upload->do_upload('bulk_recipients')){

                            if($this->input->post('now') == 'Yes'){

                                $data=array(
                                        'message'=>trim($this->input->post('message')),
                                        'filename'=>$orgname,
                                        'org_filename'=>$_FILES['bulk_recipients']['name'],
                                        'orgid'=>$this->session->userdata('org'),
                                        'createdby'=>$this->session->userdata('user_id'),
                                        'senderid'=>$this->input->post('sender'),
                                        'fext'=>$excel_ext,
                                        'smstype'=>$this->input->post('msgcategory'),
                                        'createdon'=>date('Y-m-d H:i:s'),
                                        'smsbatch'=>$sms_batch,
                                        'status'=>2,
                                        'scheduletime'=>date('Y-m-d H:i:s'),
                                        );

                                
                                if(!$this->Administrator_model->save_file_data($data)){

                                        $err_string .="$orgname";
                                    }
                            }else{
                                for($i=1;$i<=$this->input->post('scheduleNo');$i++){

                                    $date=$this->input->post('schedule'.$i);
                                    $time=$this->input->post('time'.$i);

                                    if($date <> '' && $time <> ''){

                                        $data=array(
                                                'message'=>trim($this->input->post('message')),
                                                'filename'=>$orgname,
                                                'org_filename'=>$_FILES['bulk_recipients']['name'],
                                                'orgid'=>$this->session->userdata('org'),
                                                'createdby'=>$this->session->userdata('user_id'),
                                                'senderid'=>$this->input->post('sender'),
                                                'fext'=>$excel_ext,
                                                'smstype'=>$this->input->post('msgcategory'),
                                                'createdon'=>date('Y-m-d H:i:s'),
                                                'smsbatch'=>$sms_batch,
                                                'status'=>2,
                                                'scheduletime'=>$date.' '.$time,
                                                );

                                        if(!$this->Administrator_model->save_file_data($data)){

                                            $err_string .="$orgname [ $date $time ],";
                                        }
                                    }
                                }
                            }

                            if($err_string == null){

                                $this->data['message']='<div class="alert alert-success " role="alert" style="text-align:center;">Submission Successfull</div>';
                            }else{
                                 $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">Saving Errors For '.  rtrim($err_string,',').'!</div>';
                            }
                    }else{

                      $this->data['message']=$this->upload->display_errors('<span class="alert alert-danger " role="alert" style="text-align:center;">','</span>');
                    }
                }else{
                   $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">No file Selected</div>'; 
                }

            }else{
                $total_msgs=0;
                for($j=1;$j<=$this->input->post('recipientsNo');$j++){
                        
                    if($this->input->post('msgcategory') === 'Personalized'){

                        $message= sprintf($this->input->post('message'),$this->input->post('var'.$j)[0],$this->input->post('var'.$j)[1],$this->input->post('var'.$j)[2],$this->input->post('var'.$j)[3]);
                    }else{
                        $message=$this->input->post('message');
                    }
                    $messagecount=ceil(strlen(trim($message))/$this->config->item('maximum_message_length'));
                    
                    $recipient=trim(ltrim($this->input->post('recipient'.$j),'+'));

                    if($recipient <> null){
                            $recipient=substr($recipient, 0,1) == 0 || strlen($recipient) == 9?'255'.substr($recipient,-9):$recipient;
                            if($this->input->post('now') == 'Yes'){
                                   $sms_txns[]=array(
                                                'SMS'=>$message,
                                                'ORGID'=>$this->session->userdata('org'),
                                                'USERID'=>$this->session->userdata('user_id'),
                                                'SENDERID'=>$this->input->post('sender'),
                                                'MSISDN'=>$recipient,
                                                'MESSAGECOUNT'=>$messagecount,
                                                'ACTIONDATE'=>date('Y-m-d H:i:s'),
                                                'SMSBATCH'=>$sms_batch,
                                                'STATUS'=>'Pending',
                                                'SCHEDULETIME'=>date('Y-m-d H:i:s'),
                                            );    
                           }else{
                                for($i=1;$i<=$this->input->post('scheduleNo');$i++){

                                   $date=trim($this->input->post('schedule'.$i));
                                   $time=trim($this->input->post('time'.$i));

                                   if($date <> '' && $time <> ''){

                                       $sms_txns[]=array(
                                            'SMS'=>$message,
                                            'ORGID'=>$this->session->userdata('org'),
                                            'USERID'=>$this->session->userdata('user_id'),
                                            'SENDERID'=>$this->input->post('sender'),
                                            'MSISDN'=>$recipient,
                                            'MESSAGECOUNT'=>$messagecount,
                                            'ACTIONDATE'=>date('Y-m-d H:i:s'),
                                            'SMSBATCH'=>$sms_batch,
                                            'STATUS'=>'Pending',
                                            'SCHEDULETIME'=>$date.' '.$time,
                                        );
                                   }
                                }
                           }
                           
                        }
                        
                }
                
                if($sms_txns <> null){
                   
                    if($this->Administrator_model->save_message_data($sms_txns)){

                            $this->Administrator_model->update_sms_batch($this->session->userdata('org'),$total_msgs); 
                            $this->data['message']='<div class="alert alert-success " role="alert" style="text-align:center;">Submission Successfull '.$err.'</div>';
                    }else{

                        $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">Submission Failure!</div>';
                    }
                }else{

                    $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">Submission Failure!</div>';
                }
                
            }
        }
        
        $this->data['senders']=$this->Administrator_model->senders($this->session->userdata('org'),NULL,'Active');
        $this->data['cls']='msg';
        $this->data['title']='Compose New Message';
        $this->data['content']='user/composeNewMessage';
        $this->load->view('user/template',$this->data);
    }
    
    function subscriptions(){
        if (!$this->check_session_account_validity()){
                //redirect them to the login page
                redirect('Auth/logout', 'refresh');
        }

        $this->data['panel']=$this->config->item('user_panel');

        if ($this->input->post('org')) {
            $key['org'] = $this->session->userdata('org');
            $this->data['org']=$this->session->userdata('org');
        }

      
        if ($this->input->post('start')) {
            $key['start'] = $this->input->post('start');
            $this->data['start']=$this->input->post('start');
        }
        
        if ($this->input->post('end')) {
            $key['end'] = $this->input->post('end');
            $this->data['end']=$this->input->post('end');
        }

       if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['org'] = $this->session->userdata('org');
            $this->data['org']=$key['org'];

            $key['start'] = $exp[3];
            $this->data['start']=$key['start'];

            $key['end'] = $exp[5];
            $this->data['end']=$key['end'];
        }

        $org =$this->session->userdata('org');
        $start =$key['start'];
        $end =$key['end'];

        $config["base_url"] = base_url() . "index.php/User/subscriptions/org_".$key['org']."_start_".$key['start']."_end_".$key['end']."/";
        $config["total_rows"] =$this->Administrator_model->subscriptions_count($org,$start,$end);
        $config["per_page"] =20;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '<span class="btn btn-info">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['data'] = $this->Administrator_model->subscriptions_info($org,$start,$end,$page,$limit);

        $this->data['title']="Subscriptions";
        $this->data['cls']='sub';
        $this->data['organizations']=$this->Administrator_model->companies();
        $this->data['content']='user/subscriptions';
        $this->load->view('user/template',$this->data);
    }
    
    function report(){
        $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity()){
                     //redirect them to the login page
                     redirect('Auth/logout', 'refresh');
             }

        $this->data['panel']=$this->config->item('user_panel');

        $this->form_validation->set_rules('startdate','Start Date','required|xss_clean');
        $this->form_validation->set_rules('enddate','End Date','required|xss_clean');
        $this->form_validation->set_rules('status','Status','xss_clean');

        if($this->form_validation->run() == true){
            
            $startdate=$this->input->post('startdate');
            $enddate=$this->input->post('enddate');
            $status=$this->input->post('status');
            
            $data=$this->Administrator_model->report($startdate,$enddate,$this->session->userdata('org'),$status);
            if($data <> NULL){
                   
                  require_once 'report/report.php';

            }else{

                $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">No data Found!</div>';
            }
        }

        $this->data['title']="Report";
        $this->data['cls']='rprt';
        $this->data['msg_status']=$this->Administrator_model->messageStatus();
        $this->data['content']='user/report';
        $this->load->view('user/template',$this->data);
    }
    
    function accountDetails(){
        if (!$this->check_session_account_validity()){

                   //redirect them to the login page
                   redirect('Auth/logout', 'refresh');
           }
            
        $this->data['panel']=$this->config->item('user_panel');
        $this->data['data']=$this->Administrator_model->account_details($this->session->userdata('org'));
        $this->data['cls']='prfl';
        $this->data['title']='Account Details';
        $this->data['content']='user/accountDetails';
        $this->load->view('user/template',$this->data);
    }
    
    function help(){
        if (!$this->check_session_account_validity()){

                   //redirect them to the login page
                   redirect('Auth/logout', 'refresh');
           }
        $this->data['panel']=$this->config->item('user_panel');   
        $this->data['title']='Help';
        $this->data['content']='user/help';
        $this->load->view('user/template',$this->data);
    }
    
    function getMessageBalance(){
        if (!$this->check_session_account_validity()){

                   //redirect them to the login page
                   redirect('Auth/logout', 'refresh');
           }
            
        echo number_format($this->Administrator_model->messageBalance($this->input->post('org'))->REMAININGCOUNT);
        exit;
    }
    
    function check_session_account_validity(){
        if (!$this->ion_auth->logged_in()){
                return FALSE;
        }

        if(!$this->ion_auth->in_group('User')){

            return FALSE;
        }

        return TRUE;
   }
}